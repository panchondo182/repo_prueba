<?php 
require_once 'class/config.php';
require_once 'class/generales_class.php';
require_once 'class/generales_validacionesCliente.php';
include ('curlWrap.php');

session_start();
$data = isset($_GET["data"]) ? $_GET["data"] : ''; 
$idreq = isset($_GET["idreq"]) ? $_GET["idreq"] : '';
//$data = $_GET["data"];
$cargo = isset($_SESSION['cargo']) ? $_SESSION['cargo'] : '';


//$postVenta = listaPostVenta($data,$DB);
 $mysqli = new mysqli($DB['host'], $DB['user'], $DB['pass'], $DB['useBI']);
 $mysqli2 = new mysqli($DB['host'], $DB['user'], $DB['pass'], $DB['usePP']);
 $query="SELECT * FROM requerimientos WHERE id='$idreq'";    
 $result=mysqli_query($mysqli, $query);

 while ($poliza = mysqli_fetch_array($result))
            {   
                $num_ticket =$poliza['idzendesk'];
				if($num_ticket!=''){
					$return = curlWrap("/tickets/".$num_ticket.".json", '', "GET");//////////////////////////////////////////////////
					//print_r($return);
                    $estado_detalle = $return->ticket->status;
                    $estado_fecha = $return->ticket->updated_at;
				}
                $fechaing= $poliza["fechaingreso"];
                $motivo = $poliza['motivo'];
        $requer = $poliza['requerimiento'];

                if($motivo=='0') $motivo='Sin Motivo';
                switch($poliza['requerimiento']){
                    case 'copia_de_poliza_o_certificado':
                        $reque='Copia de p&oacute;liza';
                        $sla = '4 días';
                        break;
                    case 'eliminacion_de_seguro':
                        $reque='Eliminar seguro';
                        $sla = '4 días';
                        break;
                    case 'devolucion_de_prima':
                        $reque='Devoluci&oacute;n de prima';
                        $sla = '11 días';
                        break;
                }
            $contEstilo=0;
            
        //seguros    
        if($poliza['flujo']=='W'){
            switch($poliza['codigoseg']){
                        case "SVBBSS019":
                            $c_nombre='Protecci&oacute;n Tradicional';
                            $icono = "seguro_tarjeta.png";
                            break;
                        case "SVBBSS017":
                            $c_nombre='Vida con Bonificaci&oacute;n';
                            $icono = "seguro_vida.png";
                            break;
                        case "SVBBSS023":
                            $c_nombre='Hogar Contenido';
                            $icono = "seguro_residencia.png";
                            break;
                        case "SVBBSS026":
                            $c_nombre='Viaje Pretegido Plus';
                            $icono = "seguro_viajes.png";
                            break;
                        case "SVBBSS030":
                            $c_nombre='Protecci&oacute;n Preferente';
                            $icono = "seguro_tarjeta.png";
                            break;
                        case "SVBBSS035":
                            $c_nombre='Seguro Automotriz';
                            $icono = "seguro_auto.png";
                            break;
                    }
                    $query3="select * from ".$poliza['codigoseg']." where id='$poliza[poliza]'";
                    //echo $query3;
                    if ($result3 = $mysqli2->query($query3)) {
                        while ($prod = $result3->fetch_assoc()) {    
   

                            $c_nomcli = $prod['nombre'];
                            $c_rut = formateo_rut(decrypt($prod['rut']));
                            $c_fecini = $prod['grabacion'];

                            $num_tarjetaExp = explode('*-*', decrypt($prod["num_tarjeta"]));
                            $num_tarjeta = "****-****-****-".$num_tarjetaExp[3];
                            $tarjeta = $prod['tarjeta'];
                            $banco = $prod['banco'];
                            $detVencimiento = explode('*-*', $prod["vencimiento"]);  
                            $vencimiento = $detVencimiento[0]."/".$detVencimiento[1];

                            //$c_nomcomp = '';//compañia
                            //$c_prima = ''; //prima bruta
                            //$c_poliza = '';//numero de poliza
                            
                            if($poliza['codigoseg']=='SVBBSS023'){
                                $c_poliza = '21994';
                                $c_nomcomp = 'Consorcio';
                                if($prod['plan']=='plan1'){
                                    $c_prima = '0.329';
                                }else{
                                    $c_prima = '0.506';
                                }
                            }
                            if($poliza['codigoseg']=='SVBBSS019'){
                                    $c_prima = '0.15';
                                    $c_poliza = '5515922';
                                    $c_nomcomp = 'Sura';                              
                            }
                            if($poliza['codigoseg']=='SVBBSS017'){
                                $c_nomcomp = 'Ita&uacute; Vida';
                                if($prod['plan']=='plan1'){
                                    $c_prima = '0.33';
                                }else{
                                    $c_prima = '0.99';
                                }
                                $c_poliza = 'En Proceso';
                            }
                            //auto
                            if($poliza['codigoseg']=='SVBBSS035'){
                                $c_poliza = $poliza['poliza'];
                                $c_prima = $prod['primaunica'];
                                $c_nomcomp = $prod['aseguradora'];;
                            }
                            //viaje buscar
                            if($poliza['codigoseg']=='SVBBSS026'){
                                $c_prima = $prod['primaunica'];
                                $c_nomcomp = 'Ita&uacute; Vida';
                                $c_poliza = 'En Proceso';
                            }
                            
                        }
                    }
        }
        if($poliza['flujo']=='V'){
        $query2="select * from codigos WHERE codigo='$poliza[codigoseg]' LIMIT 1";
        if ($result2 = $mysqli->query($query2)) {
        while ($codigo = $result2->fetch_assoc()) {   

            $c_nombre=$codigo['nombre'];
            $c_plan=$codigo['plan'];
            $c_tipo=$codigo['categoria'];
            switch($c_tipo){           
                case "Seguros de Proteccion Financiera":
                    $icono = "seguro_tarjeta.png";
                    break;
                case "Seguros de Vida":
                    $icono = "seguro_vida.png";
                    break;
                case "Seguros de Salud":
                    $icono = "seguro_salud.png";
                    break;
                case "Seguros de Hogar":
                    $icono = "seguro_residencia.png";
                    break;
                case "Seguros de Asistencias":
                    $icono = "seguro_viajes.png";
                    break;
                case "Seguro Automotriz":
                    $icono = "seguro_auto.png";
                    break;
                default:
                    $icono = "";
                    break;
            }
        }
        $result2->free();
    }
    $query2="select * from seguros WHERE cod_producto='$poliza[codigoseg]' and rut='$poliza[rut]' and poliza='$poliza[poliza]' LIMIT 1";
    //echo $query2;
        if ($result2 = $mysqli->query($query2)) {
        while ($codigo = $result2->fetch_assoc()){  

            $c_prima=$codigo['bruta'];
            $c_nomcli = $codigo['nombre']." ".$codigo['apellidos'];
            $c_rut = formateo_rut(decrypt($codigo['rut']));
            $c_fecini = $codigo['f_inicio'];
            $c_nomcomp = $codigo['nom_compania'];
            $c_poliza = $codigo['poliza'];

            //no informado
            $num_tarjeta = 'No informado';
            $tarjeta = 'No informado';
            $banco = 'No informado';
            $vencimiento = 'No informado';

            //si no existe nombre
            if($c_nombre=='No tiene' or $c_nombre=='') $c_nombre = $codigo['nom_producto'];
        }
        $result2->free();
    }
}
 } 
 
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Portal de Seguros</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9"/>
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="assets/css/postventa.css" rel="stylesheet" type="text/css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="assets/js/jquery-function.js"></script>   
</head>
<body>
    <header>
        <div class="logo_itau">
           <img src="assets/img/logo-itau.png" alt="">

        </div>
        <div class="header_right">
            Plataforma Comercial
        </div>
    </header>
    <div class="container">
        <div class="container_menu">
            <div class="menu">
                 <div class="top_menu"><h4>Portal de Seguros</h4></div>
                 <div class="menu_box">
                     <ul>
                            <li class="margen-bottom-10"><a href="listaSeguros.php?data=<?php echo $data;?>">Seguros contratados</a></li>
                            <li class="margen-bottom-10"><a href="bpostventa.php?data=<?php echo $data; ?>"><strong class="menu_active">Post-venta</a></strong></li>
                        </ul>
                </div>
            </div>
        </div>
        <?php //include('menuLateral.php');?>
        <div class="modulo">
            <div class="titulo">
            <h4 class="blanco">Estado</h4>
            </div>
            <div class="contenido">
                <div class="top_seguros">
                    <div class="top_left">
                        <img src="assets/img/<?php echo $icono; ?>" alt="">
                        <h1 class="top_titulo"><?php echo $c_nombre; ?></h1>
                        <p class="texto_primario font14"><strong>Requerimiento:</strong> <?php echo $reque; ?></p>
                    </div>
                    <div class="top_vig">
                        <p class="text_right fontbold font14">N° de ticket: <?php if($num_ticket==''){echo 'Pendiente de firma';}else{echo $num_ticket;}?></p>
                    </div>          
                </div>
                <div class="resumen">

                    <div class="marginr8">
                        <h4>Datos Contratante</h4>
                           <ul class="nobullet nopadding font14">
                               <li><strong>Nombre: </strong><?php echo $c_nomcli; ?></li>
                               <li><strong>RUT: </strong><?php echo $c_rut; ?></li>
                               <li><strong>Fecha de contratación: </strong> <?php echo fecha_normal($c_fecini); ?></li>
                           </ul>
                    </div>
                    <div class="marginr8">
                        <h4>Datos Póliza</h4>
                        <ul class="datos nobullet nopadding font14">
                           <li><strong>Compañía:</strong> <?php echo $c_nomcomp; ?></li>
                           <li><strong>Prima bruta mensual:</strong> UF <?php echo str_replace('.', ',', $c_prima)?> </li>
                           <li><strong>N°de Póliza: </strong><?php echo $c_poliza; ?></li>
                        </ul>
                    </div>
                    <div>
                        <h4>Datos de pago</h4>
                        <ul class="datos nobullet nopadding font14">
                            <li><strong>Tarjeta N°: </strong><?php echo $num_tarjeta;?></li>
                            <li><strong>Tipo de tarjeta: </strong><?php echo $tarjeta?></li>
                            <li><strong>Banco emisor: </strong><?php echo $banco?></li>
                            <li><strong>Fecha de vencimiento: </strong><?php echo $vencimiento;?></li>
                        </ul>
                    </div>
                </div>
                
                
                <div class="margint3">
                    <h4>Estado</h4>
                    <p class="font14">Tiempo estimado de resolución: <strong><?php echo $sla;?> hábiles</strong></p>
                    <!--<div class="clearb"></div>                    
                    <div class="progressbarPV margint2">
                        <ul class="progressPV nopadding">
                          <li class="activePV"><span>Abierto <br><?php //echo fecha_normal($fechaing); ?></span>
                          </li>
                          <li><em class="separadorPV"></em></li>
                          <li><span>En atención</span>
                          </li>
                          <li><em class="separadorPV"></em></li>
                          <li>
                          <span>Resuelto</span>
                          </li>
                        </ul>
                    </div>-->
					<div class="progressbarPV margint2">
                        <ul class="progressPV nopadding">
                          
                          <?php 
                          /*
                          switch($estado_detalle){
                            case 'new':
                                $pasouno = 'class="activePV"';
                                $fechauno = $estado_fecha;
                                break;
                            case 'open':
                                $pasodos = 'class="activePV"';
                                $fechados = $estado_fecha;
                                break;
                            case 'pending':
                                $pasotres = 'class="activePV"';
                                $fechatres = $estado_fecha;
                                break;
                            case 'solved':
                                $pasocuatro = 'class="activePV"';
                                $fechacuatro = $estado_fecha;
                                break;
                            case 'closed':
                                $pasocinco = 'class="activePV"';
                                $fechacinco = $estado_fecha;
                                break;
                            case 'hold':
                                $pasoseis = 'class="activePV"';
                                $fechaseis = $estado_fecha;
                                break;    
                            }  
                            */
                          ?>
                          <?php 
                          //echo 'estado es: '.$estado_detalle;
                         if(!empty($estado_detalle) || $estado_detalle!=''){
                          if($estado_detalle=='new'){
                                //abierto?>
                              <li class="activePV"><span>Abierto <br><?php echo fecha_normal($estado_fecha); ?></span></li>
                              <li><em class="separadorPV"></em></li>
                              <li><span>En atención</span></li>
                              <li><em class="separadorPV"></em></li>
                              <li><span>Resuelto</span></li>
                          <?php }elseif($estado_detalle=='open') {
                              //en atencion  ?>
                              <li class="activePV"><span>Abierto</span></li>
                              <li><em class="separadorPV separadorActivePV"></em></li>
                              <li class="activePV"><span>En atención <br><?php echo fecha_normal($estado_fecha); ?></span></li>
                              <li><em class="separadorPV"></em></li>
                              <li><span>Resuelto</span></li>
                          <?php }elseif($estado_detalle=='pending') {
                               //en atencion ?>
                              <li class="activePV"><span>Abierto </span></li>
                              <li><em class="separadorPV separadorActivePV"></em></li>
                              <li class="activePV"><span>En atención <br><?php echo fecha_normal($estado_fecha); ?></span></li>
                              <li><em class="separadorPV"></em></li>
                              <li><span>Resuelto</span></li>
						  <?php }elseif($estado_detalle=='hold') {
                               //en atencion ?>
                              <li class="activePV"><span>Abierto </span></li>
                              <li><em class="separadorPV separadorActivePV"></em></li>
                              <li class="activePV"><span>En atención <br><?php echo fecha_normal($estado_fecha); ?></span></li>
                              <li><em class="separadorPV"></em></li>
                              <li><span>Resuelto</span></li>
                          <?php }elseif($estado_detalle=='solved') {
                              //resuelto  ?>
                              <li class="activePV"><span>Abierto </span></li>
                              <li><em class="separadorPV separadorActivePV"></em></li>
                              <li class="activePV"><span>En atención</span></li>
                              <li><em class="separadorPV separadorActivePV"></em></li>
                              <li class="activePV"><span>Resuelto <br><?php echo fecha_normal($estado_fecha); ?></span></li>
                          <?php }elseif($estado_detalle=='closed') {
                              //resuelto  ?>
                              <li class="activePV"><span>Abierto </span></li>
                              <li><em class="separadorPV separadorActivePV"></em></li>
                              <li class="activePV"><span>En atención</span></li>
                              <li><em class="separadorPV separadorActivePV"></em></li>
                              <li class="activePV"><span>Resuelto <br><?php echo fecha_normal($estado_fecha); ?></span></li>
                          <?php }
                          }?>
                        </ul>
                    </div>
                </div>

                <?php if($requer!=='copia_de_poliza_o_certificado'){?>
                <div class="margint3">
                    <h4>Motivo</h4>
                    <p class="font14 margint2"><?php echo buscar_motivo($motivo); ?></p>
                </div>
                <?php }?>

                <a class="btn_gris margins0 displayb margint3" href="javascript:history.back(-1);">Volver</a>           
            </div>           
        </div>
    </div>     
</body>
</html>