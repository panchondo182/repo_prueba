<?php
session_start();
$cargo = isset($_SESSION['cargo']) ? $_SESSION['cargo'] : '';
$data = isset($_GET["data"]) ? $_GET["data"] : '';
switch($cargo){
    case 'EJECUTIVO':
        //header("location: segurosContratados.php?data=".$data);
        break;
    case 'AD_CTACTE':
        //header("location: segurosContratados.php?data=".$data);
        break;
	case 'JE_OPERA':
        //header("location: segurosContratados.php?data=".$data);
        break;
	case 'TESORERO':
        //header("location: segurosContratados.php?data=".$data);
        break;
    case 'AGENTE':
        //header("location: segurosContratados.php?data=".$data);
        break;
    case 'BTEL':
        header("location: listaSeguros.php?data=".$data);
        break;
    case '':
        session_destroy();
        header("location: mensajeError.php?codigo=3535");
        break;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv= "X-UA-Compatible" content="IE=9"/>
	<link href="https://itauaotbot.azurewebsites.net/Content/botchat.css" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <title>Portal de Seguros</title>
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">

    <?php
    require_once 'class/config.php';
    require_once 'class/generales_class.php';
    require_once 'class/generales_validacionesCliente.php';

    $_SESSION['rut_cliente2'] = $_SESSION['rut'];

    $data = isset($_GET["data"]) ? $_GET["data"] : '';
    $rut=decrypt($data);


    if($data!=''){
        include('class/datosClienteNew.php');
    }else{
        header("location: index.php");
    }
    ?>

</head>
<body>

<script type="text/javascript">
function cerrar() {
	//alert("aaaa");
ventana=window.self;
ventana.opener=window.self;
ventana.close();
}
</script>
    <header>
        <div class="logo_itau">
           <img src="assets/img/logo-itau.png" alt="">

        </div>
        <div class="header_right">
            Plataforma Comercial
        </div>
    </header>
    <div class="clear"></div>
    <div class="container">
	<div class="wc-div-click">
    <button class="wc-button-click">¿Dudas?</button>
  </div>
  <div id="bot" />
    <script src="https://itauaotbot.azurewebsites.net/scripts/botchat.js"></script>
    <script>
	    $(document).ready(function () {
	        $('.wc-console').css('display', 'none');
	        $('.wc-button-click, .wc-minimize-click').click(function () {
	            $('.wc-chatview-panel').toggle();
	        })
	    });

      const params = BotChat.queryParams(location.search);
      const user = {
        id: params['userid'] || 'userid',
        name: params['Tú'] || 'Tú'
      };

      const bot = {
        id: params['botid'] || 'botid',
        name: params['Mapfre'] || 'Mapfre'
      };

      window['botchatDebug'] = params['debug'] && params['debug'] === 'true';

      const botConnection = new BotChat.DirectLine({
        domain: params['domain'],
        secret: 'bp-WJPiU1nk.cwA.aU0.Y_d7YWlkXCd5wDLJLPALJ-Y5Rbzj-oQh7un6c5LpL-4',
        token: params['t'],
        webSocket: params['webSocket'] && params['webSocket'] === 'true' // defaults to true
      });

      BotChat.App({
        bot: bot,
        botConnection: botConnection,
        locale: 'es-es',
        resize: 'detect',
        user: user
      }, document.getElementById('bot'));

      botConnection.postActivity({ type: 'message', text: 'hi_box|Hola', from: user }).subscribe();

	  botConnection.activity$
	        .filter(function (activity) {
	            return activity.type === 'event' && activity.name === 'need_assist';
	        })
	        .subscribe(function (activity) {
	            if (activity.value === "dont_need_assist") {
	                $('.wc-console').css('display', 'none');
	            }
	            else {
	                $('.wc-console').css('display', 'block');
	            }
	        });

    </script>
        <?php include('menuLateral.php');?>
        <div class="modulo">
            <nav>
                <ul class="tabs">
                    <li class="tabs_active margen-bottom-10"><a href="#" id="tab1">Información del cliente</a></li>
                    <li class="margen-bottom-10"><a href="segurosContratados.php?data=<?php echo $data; ?>" id="tab2">Seguros contratados</a></li>
					<li class="margen-bottom-10"><a href="postventa.php?data=<?php echo $data; ?>" id="">Post-venta</a></li>
                    <li class="margen-bottom-10"><a href="DPS" id="">DPS</a></li>
                </ul>
            </nav>
            <div class="contenido">
                <div class="info_cliente">
                    <table class="table_left marginr2">
                        <tr>
                            <td><h1><?php echo $datos['nom']." ".$datos['apep']." ".$datos['apem']; ?></h1></td>
                        </tr>
                        <tr>
                            <td><?php echo comun_formatoPuntoRut($rut); ?></td>
                        </tr>
                        <tr>
                            <td><?php echo $datos["seg"]; ?></td>
                        </tr>
                    </table>
                    <table class="table_middle" style="display: inline-table">
                        <tr style="display: table-row" >
                            <td><strong>Edad:</strong> <?php if($datos['fna']=='' or $datos['fna']=='.') echo "No Informado"; else echo $datos["edad"]." Años"; ?></td>
                        </tr>
                        <tr style="display: table-row" >
                            <td><strong>Profesión:</strong> <?php if($datos['prof']=='' or $datos['prof']=='.') echo "No Informado"; else echo $datos['prof']; ?></td>
                        </tr>
                            <?php
                            switch($datos["trab"]){
                            	case 'SIN DESCRIPCION':
                            		$trab = 'No Informado';
                            		$empr = 'No Informado';
                            		break;
                            	case 'SIN INFORMACION':
                            		$trab = 'No Informado';
                            		$empr = 'No Informado';
                            		break;
                            	case '':
                            		$trab = 'No Informado';
                            		$empr = 'No Informado';
                            		break;
                            	case 'EMP. GRANDES EMPRESA':
                            		$trab = 'Dependiente';
                            		$empr = 'No Informado';
                            		break;
                            	case 'EMPLEADO DE PYMES':
                            		$trab = 'Dependiente';
                            		$empr = 'No Informado';
                            		break;
                            	case 'PROF.INDEPE.NO TRADI':
                            		$trab = 'Independiente';
                            		$empr = 'No Informado';
                            		break;
                            	case 'PROF.INDEPE.TRADICI.':
                            		$trab = 'Independiente';
                            		$empr = 'No Informado';
                            		break;
                            	case '0':
                            		$trab = 'No Informado';
                            		$empr = 'No Informado';
                            		break;
                            	default:
                            		$trab = 'Dependiente';
                            		$empr = $datos["trab"];
                            		break;
                            }
                            ?>
                        <tr style="display: table-row" >
                            <td><strong>Trabajador:</strong> <?php echo $trab; ?></td>
                        </tr>
                        <tr style="display: table-row" >
                            <td><strong>Empresa:</strong> <?php echo $empr; ?></td>
                        </tr>
                            <?php
                            switch($datos["eciv"]){
                                case 'CC PART.GANA':
                                    $estcivil='Casado participación de los gananciales';
                                    break;
                                case 'SOLTERO/A':
                                    $estcivil='Soltero/a';
                                    break;
                            	case 'CCSB (CAS.SE':
                                    $estcivil='Casado con separación de bienes';
                                    break;
                            	case 'CONY (CAS.SO':
                                    $estcivil='Casado en sociedad conyugal';
                                    break;
                            	case 'VIUDO/A':
                                    $estcivil='Viudo/a';
                                    break;
                            	case 'SIN INFORMAC':
                                    $estcivil='No Informado';
                                    break;
                            	case 'CRPG (CAS.PA':
                                    $estcivil='Casado participación de los gananciales';
                                    break;
                            	case 'DIVORCIADO':
                                    $estcivil='Divorciado';
                                    break;
                            	case 'CC.SEP.BIEN':
                                    $estcivil='Casado con separación de bienes';
                                    break;
                            	case 'CC.SOC.CONY':
                                    $estcivil='Casado en sociedad conyugal';
                                    break;
                            	case 'CAS C/REG PA':
                                    $estcivil='Casado participación de los gananciales';
                                    break;
                            	case 'CASADO SEP.':
                                    $estcivil='Casado con separación de bienes';
                                    break;
                            	case 'CASADO SOC.':
                                    $estcivil='Casado en sociedad conyugal';
                                    break;
                            }
                            ?>
                        <tr style="display: table-row" >
                            <td><strong>Estado Civil: </strong><?php echo $estcivil; ?></td>
                        </tr>
                    </table>
                    <table class="table_right" style="display: inline-table">

                        <tr style="display: table-row" >
                            <td><strong>Email:</strong> <?php if($datos['mail']=='' or $datos['mail']=='.') echo "No Informado"; else echo $datos['mail']; ?></td>
                        </tr>
                        <tr  style="display: table-row" >
                            <td><strong>Tel. Personal:</strong> <?php if($datos['tel4']=='' or $datos['tel4']=='.') echo "No Informado"; else echo $datos['tel4']; ?></td>
                        </tr>
						<tr  style="display: table-row" >
                            <td><strong>Tel. Particular:</strong> <?php if($datos['tel1']=='' or $datos['tel1']=='.') echo "No Informado"; else echo $datos['tel1']; ?></td>
                        </tr>
                        <tr  style="display: table-row" >
                            <td><strong>Tel. Comercial:</strong> <?php if($datos['tel2']=='' or $datos['tel2']=='.') echo "No Informado"; else echo $datos['tel2']; ?></td>
                        </tr>
                        <!-- <tr>
                            <td><strong>Ejecutivo:</strong> <?php echo $_SESSION['apenom']; ?></td>
                        </tr> -->
                        <tr  style="display: table-row" >
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>
                <?php
                 if(substr($rut, 0, -1)<=50000000){
                    echo "<input type='hidden' value='".substr($rut, 0, -1)."'>";                
                ?>
                <div class="seguros_recomendados">
                    <h3 class="margintb">Seguros recomendados</h3>
                    <div class="modulo_seguros">
                        <div class="titulo_seguro">
                            <i class="i_vida"></i>Vida
                        </div>
                        <div class="box_seguro">
                            <ul class="bullet">
                                <li><a href="productos/SVBBSS017/informacion.php?data=<?php echo $data;?>" class="underline">Seguros VD - Vida con Bonificación por Permanencia Clásico</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="modulo_seguros">
                        <div class="titulo_seguro">
                            <i class="i_proteccion"></i>Protección
                        </div>
                        <div class="box_seguro">
                             <ul class="bullet">
                                <li><a href="productos/SVBBSS019/informacion.php?data=<?php echo $data; ?>" class="underline">Protección Tradicional Full <span class="dcto"> (nuevo, 2 cuotas gratis)</span></a></li>
                                <li><a href="productos/SVBBSS030/informacion.php?data=<?php echo $data; ?>" class="underline">Protección Preferente Full <span class="dcto"> (nuevo, 2 cuotas gratis)</span></a></li>
                                <!-- <li>Desgravamen + ITO 2/3 individual</li> -->
                            </ul>
                        </div>
                    </div>
                    <div class="modulo_seguros">
                        <div class="titulo_seguro">
                            <i class="i_asistencias"></i>Asistencias
                        </div>
                        <div class="box_seguro">
                            <ul class="bullet">
                                <li><a href="productos/SVBBSS026/informacion.php?data=<?php echo $data; ?>" class="underline">Viaje Protegido Plus <span class="dcto"> (dcto. 30%)</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="modulo_seguros">
                        <div class="titulo_seguro">
                            <i class="i_hogar"></i>Hogar
                        </div>
                        <div class="box_seguro">
                            <ul class="bullet">
                               <li><a href="productos/SVBBSS023/informacion.php?data=<?php echo $data; ?>" class="underline">Hogar Contenido</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="modulo_seguros">
                        <div class="titulo_seguro">
                            <i class="i_auto"></i>Automotriz
                        </div>
                        <div class="box_seguro">
                            <ul class="bullet">
                               <li><a href="productos/SVBBSS035/informacion.php?data=<?php echo $data; ?>" class="underline">Seguro Automotriz</a></li>
                            </ul>
                        </div>
                    </div>
					<div class="modulo_seguros">
                        <div class="titulo_seguro">
                            <i class="i_salud"></i>Salud
                        </div>
                        <div class="box_seguro">
                            <ul class="bullet">
                                <li><a href="productos/SVBBSS022/informacion.php?data=<?php echo $data; ?>" class="underline">Altos Gastos Médicos Plus</a></li>
                            </ul>
                        </div>
                    </div>

                    <!--<a class="btn_gris displayb margins0" href="index.php?return=true">Volver</a>  -->
                 </div> <?php } else{ ?>
                    <div style="
    margin-top: 30px;
    text-align: center;
"> <h3 class="centrar">Los seguros en línea sólo pueden ser contratados por Personas Naturales.</h3></div>
                 <?php } ?>
            </div>
        </div>
    </div>
</body>
</html>
