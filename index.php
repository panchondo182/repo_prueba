<?php
    session_start();

    if(isset($_POST["AO_Token"])){
        $_SESSION['estado']         = '';
        $_SESSION['user']           = '';
        $_SESSION['descripcion']    = '';
        $_SESSION['token']          = '';
        $_SESSION['incremental']    = '';
        $_SESSION['userid']         = '';
        $_SESSION['apenom']         = '';
        $_SESSION['cargo']          = '';
        $_SESSION['suc']            = '';
        $_SESSION['nomsuc']         = '';
        $_SESSION['rut']            = ''; 
        $_SESSION['pc'] 			= '';
        $_SESSION['UrlValidaPinItau'] 			= '';
        $_SESSION['serverName']     = '';
        $_SESSION['UrlVolver'] 			= 'http://itaucomercialqa.alwayson.cl/';
    }
    require_once 'class/config.php';
    require_once 'class/generales_class.php';
    require_once 'class/generales_validacionesCliente.php';
    
    $rut = isset($_POST["rut-cliente"]) ? $_POST["rut-cliente"] : '';
	$rut = str_replace("-","",$rut);

    if ($rut !='') {
        $mensaje= "";
        
        $valRut = isset($_POST["valRut"]) ? $_POST["valRut"] : ''; 
		$valRut = str_replace("-","",$valRut);		
        if($rut!='' && $valRut=='ok'){
          $run = $_SESSION['rut'];
            
            
            $datos=comun_verificaCliente(encrypt($run),$DB);
//            var_dump($datos);
            if($datos['respuesta'] == 'rut ok'){
                //$datosEncriptados= serialize($datos);
                //$datosEncriptados=str_replace('"', '|',$datosEncriptados); 
                //header("location: informacion.php?data=".encrypt($datosEncriptados));
                $_SESSION['rut_ejecutivo'] = $_POST['rut-cliente'];
                header("location: informacion.php?data=".encrypt($run));                
            }else{
                //PAGINA ERROR RUT CLIENTE NO CORRESPONDE
                //header("location: mensajeError.php?codigo=8383");
                header("location: login.php");
            }
        }
    }else{

        $token = isset($_POST["AO_Token"]) ? $_POST["AO_Token"] : '';
        $valor = isset($_SESSION["rut"]) ? $_SESSION["rut"] : '';
        $retorno = isset($_GET["return"]) ? $_GET["return"] : '';

        //DATOS PARA LA VALIDACION DEL TOKEN

        $semilla = "1234abcd";
        $userid = $_POST['AO_UserID'];
        $rut = $_POST['AO_rut'];
		$rut= str_replace("-","",$rut);
        $dif = $_POST['AO_Incremental'];
        
        $encript_md5 = md5($userid.$dif.$rut.$semilla);
		
        
        //echo $encript_md5;
        
        
        
        if($token == $encript_md5 || $token == 'V1ct0r.C0fr3'){
            $cargo = isset($_POST["AO_Cargo"]) ? $_POST["AO_Cargo"] : '';
            if($cargo=='BTEL'){
                $_SESSION['estado']             = $_POST['Estado'];
                $_SESSION['descripcion']        = $_POST['Descripcion'];
                $_SESSION['token']              = $_POST['AO_Token'];
                $_SESSION['incremental']        = $_POST['AO_Incremental'];
                $_SESSION['userid']             = $_POST['AO_UserID'];
                $_SESSION['apenom']             = $_POST['AO_Apenom'];
                $_SESSION['cargo']              = $_POST['AO_Cargo'];
                $_SESSION['suc']                = $_POST['AO_Suc'];
                $_SESSION['nomsuc']             = $_POST['AO_NomSuc'];
                $_SESSION['rut']                = $_POST['AO_rut'];
                $_SESSION['user']               = $_POST['AO_user'];
                $_SESSION['serverName']         = $_POST['AO_serverName'];
                $_SESSION['UrlValidaPinItau']   = $_POST['AO_UrlVolver'];;
                //$_SESSION['UrlVolver'] 			= 'http://itaucomercial.alwayson.cl/productos/';
				$_SESSION['UrlVolver'] 			= 'http://itaucomercialqa.alwayson.cl/';
                header("location: listaSeguros.php?data=".encrypt($rut));
            }
        }else{
            //header("location: mensajeError.php?codigo=3539");
            header("location: login.php");
        }
        
        if(strlen($token) != "" || strlen($_SESSION['rut_cliente2'] != "")){


            if($retorno == "true"){

            }elseif($retorno == ''){

                $estado = $_POST['Estado'];
                $descripcion = $_POST['Descripcion'];
                $token = $_POST['AO_Token'];
                $incremental = $_POST['AO_Incremental'];
                $userid = $_POST['AO_UserID'];
                $apenom = $_POST['AO_Apenom'];
                $cargo = $_POST['AO_Cargo'];
                $suc = $_POST['AO_Suc'];
                $nomsuc =$_POST['AO_NomSuc'];
                $rut = $_POST['AO_rut'];
                $serverName = $_POST['AO_serverName'];
				$rut= str_replace("-","",$rut);
                $user = $_POST['AO_user'];
                $UrlValidaPinItau = $_POST['AO_UrlValidaPinItau'];
                $UrlVolver = $_POST['AO_UrlVolver'];
                $_SESSION['estado']         = $estado;
                $_SESSION['descripcion']    = $descripcion;
                $_SESSION['token']          = $token;
                $_SESSION['incremental']    = $incremental;
                $_SESSION['userid']         = $userid;
                $_SESSION['apenom']         = $apenom;
                $_SESSION['cargo']          = $cargo;
                $_SESSION['suc']            = $suc;
                $_SESSION['nomsuc']         = $nomsuc;
                $_SESSION['rut']            = $rut; 
                $_SESSION['user']            = $user;
                $_SESSION['serverName']     = $serverName;
                $_SESSION['pc']             ='';
                $_SESSION['UrlValidaPinItau'] =$UrlValidaPinItau;
                //$_SESSION['UrlVolver'] 			= 'http://itaucomercial.alwayson.cl/productos/';
				$_SESSION['UrlVolver'] 			= 'http://itaucomercialqa.alwayson.cl/';
            }

    
            

        }else{
            if($retorno == "true" && strlen($_SESSION['rut_cliente2'] != NULL )){

             

            }else{
                     //header("location: mensajeError.php?codigo=3535");
                     header("location: login.php");
                     //session_destroy();
            }
        }
    }
          

    ?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv= "X-UA-Compatible" content="IE=9"/>
    <title>Plataforma de Seguros</title>
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css">
    
<!--
    <script src="assets/js/valida_herederos.js" type="text/javascript" ></script>
    <script src="assets/js/pago.js" type="text/javascript" ></script>
-->
    <?php echo "Estado " . $estado . "Descricpion " . $descripcion; ?>
</head>
<body>
    <header>
        <div class="logo_itau">
            <img src="assets/img/logo-itau.png" alt="">

        </div>
        <div class="header_right">
            Plataforma Comercial
           <!-- -->
        </div>
    </header>
    
    <div class="bg_home">
        <div class="box_home">
            <div>
                <h2 class="text_center">¡Bienvenid@ a la Plataforma de Seguros!</h2>
                <p class="text_center margint2">
                        <i>Ingresa tu RUT para comenzar la sesión</i>
                </p>
                <form style="height: 120px;" action="<?php $_SERVER['PHP_SELF'];?>" id="busquedaClientes" name="busquedaClientes" method="POST">
                    <div class="margint3" style="height: 90px;">
                        <label class="marginr3 font14"><strong>RUT</strong></label>
                        <input id="rut-cliente"  name="rut-cliente" ondrop="return false;" onpaste="return false;" type="text" maxlength="9" class="input_login floatr displayb" autocomplete="off" maxlength="10" value="" onkeypress="return soloNumeros(event);">

                        <div class="clear" style="margin-bottom: 15px; margin-top: 15px;"></div>
                        <label class="font14"><strong>Confirmar RUT </strong></label>
                        <input id="rut-cliente2" name="rut-cliente2" ondrop="return false;" onpaste="return false;" type="text" maxlength="9" class="input_login floatr displayb" autocomplete="off" maxlength="10" value="" onkeypress="return soloNumeros(event);">

                    </div>

                    <input style="display:none;" name="valRut" id="valRut" value="">
                    <input style="display:none;" name="rut_cli" id="rut_cli" value="<?php if(isset($_POST['AO_rut'])){ echo $_POST['AO_rut']; }else{ echo $_SESSION['rut_cliente2']; } ?>">
					<input style="display:none;" name="token" id="token" value="<?php if(isset($_POST['AO_Token'])){ echo $_POST['AO_Token']; }else{ echo "sintokepost"; } ?>">
					<input style="display:none;" name="token2" id="token2" value="<?php echo $encript_md5; ?>">
					
					<input style="display:none;" name="urlback" id="urlback" value="<?php echo $_POST['AO_UrlValidaPinItau']; ?>">
					<input style="display:none;" name="AO_serverNamex" id="AO_serverNamex" value="<?php echo $_POST['AO_serverName'];; ?>">
					<input style="display:none;" name="AO_Apenomx" id="AO_Apenomx" value="<?php echo $_POST['AO_Apenom']; ?>">
					<input style="display:none;" name="AO_UserIDx" id="AO_UserIDx" value="<?php echo $_POST['AO_UserID']; ?>">
					<input style="display:none;" name="AO_rutx" id="AO_rutx" value="<?php echo $_POST['AO_rut']; ?>">
					<input style="display:none;" name="AO_NomSucx" id="AO_NomSucx" value="<?php echo $_POST['AO_NomSuc']; ?>">
                    
                    <div id="errRut" class="errorRutInicio" style="line-height: 12px;"></div>
          
             </form>

                <a href="#" class="btn_naranja floatr displayb" style="margin-top: 10px;" onclick="envia()">Iniciar Sesión</a>
            </div>

        </div>
    </div>

    
    
    <script src="assets/js/validaRut.js" type="text/javascript" ></script>
    <script type="text/javascript">
    var input = document.getElementById("rut-cliente");
    var input2 = document.getElementById("rut-cliente2");
    var mensajeError = document.getElementById('errRut');

      function errorCampoVacio(rut){
      if (rut.value==''){
       mensajeError.className = 'errorRutInicio';
        document.getElementById('errRut').innerHTML="<h5>*Debes ingresar tu RUT</h5>";
        document.getElementById('valRut').value='';
        //document.getElementById('rut-cliente').className = "errorRutInput";
      }
    }
    
    
    input.addEventListener('change', function( event ){
      var valor = this.value;
      var rut = new validaRut( valor );

      var mensajeError = document.getElementById('errRut');
      mensajeError.className = 'errorRutInicio';

      if( !rut.isValid ){
        // alert('El rut no es valido');
        /**
        mensajeError.className = 'errorRutInicio';
        document.getElementById('errRut').innerHTML="";
        document.getElementById('valRut').value='error';
        //document.getElementById('rut-cliente').className = "errorRutInput";
        */
      }
      else {
        // alert('el rut es valido');
        //document.getElementById('rut-cliente').className = "ok";
        document.getElementById('errRut').innerHTML="";
        document.getElementById('valRut').value='ok';
        mensajeError.className = 'errorRutInicio';
        console.log(rut.cleanRut);
      }
      
      this.value = rut.cleanRut;
    });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////// VALIDA RUT 2 ////////////////////////////////////////////////

    input2.addEventListener('change', function( event ){
      var valor = this.value;
      var rut = new validaRut( valor );

      var mensajeError = document.getElementById('errRut');
      mensajeError.className = 'errorRutInicio';

      if(!rut.isValid ){
        // alert('El rut no es valido');
        /**
        mensajeError.className = 'errorRutInicio';
        document.getElementById('errRut').innerHTML="<h5>*RUT ingresado inválido</h5>";
        document.getElementById('valRut').value='error';
        //document.getElementById('rut-cliente').className = "errorRutinput";
        */
      }
      else {

        document.getElementById('errRut').innerHTML="";
        document.getElementById('valRut').value='ok';
        mensajeError.className = 'errorRutInicio';
        console.log(rut.cleanRut);
      }
      
      this.value = rut.cleanRut;
    });

    function envia(){

        var rut1 = document.getElementById("rut-cliente").value;
        var rut2 = document.getElementById("rut-cliente2").value;

        var run_cliente = document.getElementById("rut_cli").value;
        

        // VALIDACION DE RUT
        var rut_validador = new validaRut( rut1 );
        var rut_validador2 = new validaRut( rut2 );
        var rut_validador3 = new validaRut(run_cliente);


            if(rut1 == "") {
                 mensajeError.className = 'errorRutInicio';
                document.getElementById('errRut').innerHTML="<h5>*Debes ingresar tu RUT</h5>";
                document.getElementById('valRut').value='error';
            }else if(rut1.length < 8){
                mensajeError.className = 'errorRutInicio';
                document.getElementById('errRut').innerHTML="<h5>*RUT ingresado inválido</h5>";
                document.getElementById('valRut').value='error';

            }else if(rut2 == ""){
                 mensajeError.className = 'errorRutInicio';
                document.getElementById('errRut').innerHTML="<h5>*Debes confirmar tu RUT</h5>";
                document.getElementById('valRut').value='error';

            }else if(rut2.length < 8){
                mensajeError.className = 'errorRutInicio';
                document.getElementById('errRut').innerHTML="<h5>*RUT ingresado inválido</h5>";
                document.getElementById('valRut').value='error';

            }else if(rut1 != rut2){
                mensajeError.className = 'errorRutInicio';
                document.getElementById('errRut').innerHTML="<h5>*Los datos ingresados no coinciden</h5>";
                document.getElementById('valRut').value='error';
            }else if(!rut_validador.isValid){
                mensajeError.className = 'errorRutInicio';
                document.getElementById('errRut').innerHTML="<h5>*RUT ingresado inválido</h5>";
                document.getElementById('valRut').value='error';
            }else if(!rut_validador2.isValid){
                mensajeError.className = 'errorRutInicio';
                document.getElementById('errRut').innerHTML="<h5>*RUT ingresado inválido</h5>";
                document.getElementById('valRut').value='error';
            }else if(!rut_validador3.isValid){
                mensajeError.className = 'errorRutInicio';
                document.getElementById('errRut').innerHTML="<h5>*RUT clientes inválido</h5>";
                document.getElementById('valRut').value='error';
            }else if(rut1 == rut2){
                document.getElementById("busquedaClientes").submit();  
            }else{
                mensajeError.className = 'errorRutInicio';
                document.getElementById('errRut').innerHTML="<h5>*Los datos ingresados no coinciden</h5>";
                document.getElementById('valRut').value='error';
            }
    }

    function validacionDatosLogin(){

        mensajeError.className = 'errorRutInicio';
            document.getElementById('errRut').innerHTML="";
            document.getElementById('valRut').value='error';
    }



// function soloNumeros(e) {
//     e = (e) ? e : window.event
//     var charCode = (e.which) ? e.which : e.keyCode
//     if (charCode > 31 && (charCode < 48 || charCode > 57)) {
//         //status = "This field accepts numbers only."
//         return false
//     }
//     status = ""
//     return true
// }


function soloNumeros(evt) {

     if(window.event){//asignamos el valor de la tecla a keynum
          keynum = evt.keyCode; //IE
         }
         else{
          keynum = evt.which; //FF
         } 
         //comprobamos si se encuentra en el rango numérico y que teclas no recibirá.
         if((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13 || keynum == 6 || keynum == 75 || keynum == 107 || keynum == 9 || keynum == 0 ){
          return true;
         }
         else{
          return false;
         }
   

}


   

  </script>



</body>
</html>
