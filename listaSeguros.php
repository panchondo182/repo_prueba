<?php include 'metadata.php';
session_start();
$cargo = isset($_SESSION['cargo']) ? $_SESSION['cargo'] : ''; 
$data = isset($_GET["data"]) ? $_GET["data"] : ''; 


switch($cargo){
    case 'EJECUTIVO':
        header("location: segurosContratados.php?data=".$data);
        break;
    case 'AD_CTACTE':
        header("location: segurosContratados.php?data=".$data);
        break;
	case 'JE_OPERA':
        header("location: segurosContratados.php?data=".$data);
        break;
	case 'TESORERO':
        header("location: segurosContratados.php?data=".$data);
        break;
    case 'AGENTE':
        header("location: segurosContratados.php?data=".$data);
        break;
    case 'BTEL':        
        //header("location: listaSeguros.php?data=".$data);
        break;
    case '':
        session_destroy();
        header("location: mensajeError.php?codigo=3535");
        break;
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv= "X-UA-Compatible" content="IE=9"/>
    <title>Plataforma de Seguros</title>
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css">
    <script src="validaRut.js" type="text/javascript" ></script>
    <script src="assets/js/valida_herederos.js" type="text/javascript" ></script>
    <script src="assets/js/pago.js" type="text/javascript" ></script>
    
    <?php
    require_once 'class/config.php';
    require_once 'class/generales_class.php';
    require_once 'class/generales_validacionesCliente.php';
    
    
    $data = isset($_GET["data"]) ? $_GET["data"] : ''; 
    
    $c_nombre='';
    $segIde='';
    
//    $data = isset($_GET["datosEncriptados"]) ? $_GET["datosEncriptados"] : '';    
//    if($data!=''){
//        $data2=str_replace('|', '"',$data );
//        $datos=unserialize($data2);
//        //var_dump($datos);
//        $dato_rut = encrypt($datos['rut']);
//        $seguro = comun_listarSeguros($dato_rut);
//   }else{
//        //echo "<script>location.href='home.php'</script>";
//    }
    
    $data = isset($_GET["data"]) ? $_GET["data"] : '';  
    $rut=decrypt($data);

    if($data!=''){
        $seguro = comun_listarSeguros(encrypt(formateo_rut(decrypt($data))),$data,$DB);
    }else{
        header("location: index.php");
    }
//$viene = $_SESSION['cargo'];
?>


</head>
 <body>
       <header>
        <div class="logo_itau">
           <img src="assets/img/logo-itau.png" alt="">

        </div>
        <div class="header_right">
            Plataforma Comercial
        </div>
    </header>
    
    <div class="container">
    
           <div class="container_menu">
                    <div class="menu">
                         <div class="top_menu"><h4>Portal de Seguros</h4></div>
                         <div class="menu_box">
                             <ul>
                                <li class="margen-bottom-10"><a href="#"><strong class="menu_active">Seguros contratados</a></strong></li>
                                <?php if(substr(decrypt($data), 0, -1)<=50000000){ ?><li class="margen-bottom-10"><a href="bpostventa.php?data=<?php echo $data;?>&viene=<?php echo encrypt($viene);?>">Post-venta</a></li><?php }?>
                             </ul>
                        </div>
                    </div>
            </div>
        
    <div class="modulo">
        <nav>
            <ul class="tabs">
                <!-- <li ><a href="informacion.php?data=<?php echo $data; ?>">
                Información del cliente</a></li> -->
                <li class="tabs_active"><a href="#">Seguros contratados</a></li>
                <?php if(substr(decrypt($data), 0, -1)<=50000000){ ?><li class="margen-bottom-10"><a href="bpostventa.php?data=<?php echo $data; ?>" id="">Post-venta</a></li><?php }?>

                <!-- <li><a href="#" id="tab3">Servicio Post-Venta</a></li> -->   
            </ul>
        </nav>

        <div class="contenido" >
            <div class="seguros_contratados">
            <table class="font14">
            <tr class="text_center azul">
            <th></th>


        <?php 
        $tituloos=1;
        $tit=false;
        foreach($seguro as $rows => $value){ 
            $segIde=$value['segIde'];
            if($tituloos==1 && $tit == false){
                $tit=true;
				echo '<th>Productos</th>
				<th>Estado</th>
				<th>PDF Referencial</th>
				</tr>';
			}
			$tituloos++;

            switch($value["segGrupo"]){           
                case "Financiero":
                    $icono = "i_proteccion";
                    break;
                case "Vida":
                    $icono = "i_vida";
                    break;
                case "Salud":
                    $icono = "i_salud";
                    break;
                case "Hogar":
                    $icono = "i_hogar";
                    break;
                case "Asistencia":
                    $icono = "i_asistencias";
                    break;
                case "Automotriz":
                    $icono = "i_auto";
                    break;
                default:
                    $icono = "";
                    break;
                        break;
            }?>

            <tr>
                <td><i class=<?php echo $icono; ?>></i></td>
                <td><ul>
                    <li>
                        <a href="productos/<?php echo $segIde; ?>/detalleContratado.php?data=<?php echo $data; ?>&resultado=<?php echo $value['contrato']; ?>&volver=8" class="underline" style="color: #595959;">
                        <strong><?php echo utf8_encode($value['segNombre']);  ?></strong></a></li>
                    <li>
                        <strong>
                        <?php if($segIde=='SVBBSS026'){
                                echo "Prima única: "; 
                            }else{
                                echo "Prima Bruta Mensual: ";
                        } ?>
                        </strong>
                        
                        <?php if($value['segPrima'] =='') echo "No Informado"; else echo "UF " . str_replace(".",",",$value['segPrima']);  ?>

                    </li>
                </ul></td>
                <td  style="text-align: center;">
                    <?php echo $value['PrimeraLinea']; ?><br><br> <?php echo $value['SegundaLinea'];  ?>
                </td>
				<?php if($segIde=='SVBBSS019'){ ?>
					<td  style="text-align: center;"><a href="productos/pdf/fraude.pdf" target="_blank" style="color:#f57200;">Descargar</a></td>
				<?php } ?>
				<?php if($segIde=='SVBBSS017'){ 
						if($segIde=='plan1'){?>
							<td  style="text-align: center;"><a href="productos/pdf/vida.pdf" target="_blank" style="color:#f57200;">Descargar</a></td>
						<?php }else { ?>
							<td  style="text-align: center;"><a href="productos/pdf/vida.pdf" target="_blank" style="color:#f57200;">Descargar</a></td>
						<?php }?>	
				<?php } ?>
				<?php if($segIde=='SVBBSS023'){ 
						if($segIde=='plan1'){?>
							<td  style="text-align: center;"><a href="productos/pdf/hogar.pdf" target="_blank" style="color:#f57200;">Descargar</a></td>
						<?php }else { ?>
							<td  style="text-align: center;"><a href="productos/pdf/hogar.pdf" target="_blank" style="color:#f57200;">Descargar</a></td>
						<?php }?>	
				<?php } ?>
				<?php if($segIde=='SVBBSS026'){ 
						if($segIde=='plan1'){?>
							<td  style="text-align: center;"><a href="productos/pdf/viaje.pdf" target="_blank" style="color:#f57200;">Descargar</a></td>
						<?php }else { ?>
							<td  style="text-align: center;"><a href="productos/pdf/viaje.pdf" target="_blank" style="color:#f57200;">Descargar</a></td>
						<?php }?>	
				<?php } ?>

                <?php if($segIde=='SVBBSS035'){ 
                        if($segIde=='plan1'){?>
                            <td  style="text-align: center;"><a href="productos/pdf/seguroAutomotriz.pdf" target="_blank" style="color:#f57200;">Descargar</a></td>
                        <?php }else { ?>
                            <td  style="text-align: center;"><a href="productos/pdf/seguroAutomotriz.pdf" target="_blank" style="color:#f57200;">Descargar</a></td>
                        <?php }?>   
                <?php } ?>
            </tr>
            <?php 
			}

            
            $mysqli = new mysqli($DB['host'],$DB['user'],$DB['pass'],$DB['useBI']);
			$query = "select * from seguros WHERE rut='$data' and corredora in ('CORPBANCA','ITAU')";
			if ($result = $mysqli->query($query)) {
                while ($poliza = $result->fetch_assoc()) {
				$query2="select * from codigos WHERE codigo='$poliza[cod_producto]' LIMIT 1";
				if ($result2 = $mysqli->query($query2)) {
                while ($codigo = $result2->fetch_assoc()) {
					$c_nombre=$codigo['nombre'];
					$c_plan=$codigo['plan'];
					$c_tipo=$codigo['categoria'];
					switch($c_tipo){           
						case "Seguros de Proteccion Financiera":
							$icono = "i_proteccion";
							break;
						case "Seguros de Vida":
							$icono = "i_vida";
							break;
						case "Seguros de Salud":
							$icono = "i_salud";
							break;
						case "Seguros de Hogar":
							$icono = "i_hogar";
							break;
						case "Seguros de Asistencias":
							$icono = "i_asistencias";
							break;
						case "Seguro Automotriz":
							$icono = "i_auto";
							break;
						default:
							$icono = "";
							break;
					}
				}
				$result2->free();
            }
				if($poliza['corredora']=='CORPBANCA'){
					$c_nombre=$poliza['nom_producto'];
					if($poliza['neta']<150){
						$netapol="UF ".str_replace(".",",",$poliza['neta']);
					}else{
						//$netapol="$ ".str_replace(".",",",$poliza['neta']);
                        $netapol=number_format($poliza['neta'],0,".",",");
                        $netapol="$ ".str_replace(",",".",$netapol);

					}
				}else{
					//$netapol="UF ".str_replace(".",",",$poliza['bruta']);                    
                    $netapol=substr($poliza['bruta'],0,5);
                    $netapol="UF ".str_replace(".",",",$netapol);  
                    //$number=number_format($netapol);                  

				}
                 if($tit == false){
                        $tit=true;
                        echo '<th>Productos</th>
                        <th>Estado</th>
                        <th>Detalle</th>
                        </tr>';
                    }
			?>
					<tr>
					<td><i class=<?php echo $icono; ?>></i></td>
					<td><ul>
                    <li>
                        <strong><?php echo $c_nombre; ?></strong></li>
                    <li>
                        <strong>Prima Bruta Mensual: </strong>
                        <?php echo $netapol; ?>
                    </li>
					</ul></td>

					<td  style="text-align: center;">
						<strong>Vigencia: </strong><?php echo substr($poliza['f_inicio'],8,2)."/".substr($poliza['f_inicio'],5,2)."/".substr($poliza['f_inicio'],0,4). " - " .substr($poliza['f_fin'],8,2)."/".substr($poliza['f_fin'],5,2)."/".substr($poliza['f_fin'],0,4); ?><br><br> <strong>Renovación automática</strong>
						 <?php if($poliza['gestion_requerimiento']==1){
                            echo '<br/>(Eliminaci&oacute;n en proceso)';
                        }?>
					</td>
					<td  style="text-align: center;">&nbsp;</td>	

                   
					</tr>
					<?php
			}
			$result->free();
			$mysqli->close();
        }
            	if($segIde == ''){

            	}else{
            		echo "</table></div></div>";
            	}

            ?>
               


        <?php

        	if($segIde == '' && $c_nombre==''){

        		echo "<div class='contenido' style='border: none;'>
                <div>
                    <div class='message margint5'>
                        <img src='assets/img/ico-seguros.png' alt='' class='margins0'>
                        <p class='text_center margintb font16'><strong>El cliente no posee seguros contratados</strong></p>

                        
                    </div>
                </div>
            </div>";


        	}
//<a class='btn_naranja displayb margintb margins0' href='informacion.php?data=".$_GET['data']."'>Ir al cliente</a>
        ?>
       </div>
    </div>
</body>
</html>
