<?php header( "refresh:3;url=login.php" ); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=9"/>
    <title>Portal de Seguros</title>

    <?php include 'metadata.php'; ?>
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css">
    <script src="validaRut.js" type="text/javascript" ></script>
    <script src="assets/js/valida_herederos.js" type="text/javascript" ></script>
    <script src="assets/js/pago.js" type="text/javascript" ></script>
    <script src="assets/js/validaDeclaracion.js" type="text/javascript" ></script>
    <?php
    require_once 'class/generales_class.php';
    require_once 'class/generales_validacionesCliente.php';
    require_once 'class/config.php';

    $data = $_GET['data'];
    require_once('class/datosClienteNew.php');

    $error = $_GET['codigo'];
    session_start();
    session_destroy();
?>
</head>
<body>
 <header>
        <div class="logo_itau">
            <img src="assets/img/logo-itau.png" alt="">
        </div>
        <div class="header_right">
            Plataforma Comercial
        </div>
        <div class="clear"></div>
    </header>

    <div class="clear"></div>

    <!-- <div class="container_menu">
        <div class="menu">
            <div class="top_menu">
                <h4>Portal de Seguros</h4>
            </div>
            <div class="menu_box">
                <ul>
                    <li><a href=""><strong class="menu_active">Perfil Asegurado</strong></a></li>
                    <li><a href="">Nuestros Productos</a></li>
                </ul>
            </div>
        </div>
    </div> -->

    <div class="modulo" style="margin: 0 auto; margin-top: 95px; width: 80%; float: none; display:block;">
        <div class="titulo">
            <h4 class="blanco">
            <?php
            if($error=="2442"){
                echo 'Proceso de Post-Venta';
            }else{
                echo 'Proceso de Ventaa';
            }?>
            </h4>
        </div>
        <div class="contenido">
            <div class="error">
                <div class="error_img">
                    <img src="assets/img/foto-error.jpg" alt="" class="">    
                </div>
                <div class="error_message_vida text_center" style="border: 0px;">
                    <i class="

                        <?php 
                            $error = isset( $_GET['codigo']) ? $_GET['codigo']:'3535';
                            if($error == "3838" || $error == "8383"){
                                echo "i_error marginb2";
                            }else{
                                echo "i_error marginb2 margint3";
                            }
                        ?> ">
                        
                    </i>

                    <?php

                        $error = isset( $_GET['codigo']) ? $_GET['codigo']:'3535';


                        if($error == "3838"){
                           echo '<div>
                        <p>El cliente no se encuentra registrado aún en esta Plataforma de Seguros. Por favor, envía un correo a tu <strong>Ejecutiva Comercial de Seguros</strong> con los siguientes datos de tu cliente para que sea ingresado en máximo <strong>48 horas</strong>:
                    </p>
                    <br>
                    
                    <ul style="text-align: left;">
                        <li style="margin-bottom: 5px;">Nombre completo*</li>
                        <li style="margin-bottom: 5px;">Rut*</li>
                        <li style="margin-bottom: 5px;">Teléfono*</li>
                        <li style="margin-bottom: 5px;">Mail*</li>
                        <li style="margin-bottom: 5px;">Fecha de nacimiento*</li>
                    </ul>
                    
                    <p style="text-align: left;">*Datos obligatorios</p>
                    
                    <p style="text-align: left;"><strong>Coordinadoras de Seguros según tu zona:</strong></p>

                    <table style="border-spacing: 1px;">
                          <tr>
                            <th style="border: solid 1px #d2d2d2; width: 15%;">Zona</th>
                            <th style="width: 35%; border: solid 1px #d2d2d2">Ejecutiva Comercial</th>
                            <th style="width: 30%; border: solid 1px #d2d2d2">División</th>
                            <th style="width: 25%; border: solid 1px #d2d2d2">Sub División</th>
                          </tr>
                          <tr>
                            <td style="border: solid 1px #d2d2d2"><strong>I</strong></td>
                            <td style="border: solid 1px #d2d2d2; text-align: left; padding: 10px;">
                                <strong>Mónica Fuentes</strong><br>
                                monica.fuentes@itau.cl<br>
                                Anexo:0348
                            </td>
                            <td style="border: solid 1px #d2d2d2;">Santiago Sur <br>Santiago Centro</td>
                            <td style="border: solid 1px #d2d2d2;"> - </td>
                          </tr>
                          <tr>
                            <td style="border: solid 1px #d2d2d2"><strong>II</strong></td>
                            <td style="border: solid 1px #d2d2d2; text-align: left; padding: 10px;">
                                <strong>Makarena Vargas</strong> <br>
                                makarena.vargas@itau.cl <br>
                                Anexo:7220
                            </td>
                            <td style="border: solid 1px #d2d2d2">Santiago Oriente <br>Personal Bank</td>
                            <td style="border: solid 1px #d2d2d2"> - </td>
                          </tr>
                          <tr >
                            <td style="border: solid 1px #d2d2d2" ><strong>III</strong></td>
                            <td style="border: solid 1px #d2d2d2; text-align: left; padding: 10px;">
                                <strong>Marjorie Silva</strong><br>
                                marjorie.silva@itau.cl<br>
                                Anexo:7221
                            </td>
                            <td style="border: solid 1px #d2d2d2; padding: 10px;">
                                Santiago Cordillera<br>
                                Región VIII<br>
                                Región Centro<br>
                                Región Sur

                            </td>
                            <td style="border: solid 1px #d2d2d2">Rancagua<br>San Fernando</td>
                          </tr>
                          <tr>
                            <td style="border: solid 1px #d2d2d2"><strong>IV</strong></td>
                            <td style="border: solid 1px #d2d2d2; text-align: left; padding: 10px;">
                                <strong>Carolina Montoya</strong><br>
                                carolina.montoya@itau.cl<br>
                                Anexo:7219
                            </td>
                            <td style="border: solid 1px #d2d2d2; padding: 10px;">
                                Santiago Poniente<br>
                                Región V<br>
                                Región Norte<br>
                                Región Centro

                            </td>
                            <td style="border: solid 1px #d2d2d2">Curic&oacute;<br>Talca</td>
                          </tr>

                        </table>
                    </div>';
                        }elseif($error == "3535"){
                            echo "<h2>Acceso restringido</h2>";
                        }elseif($error == "2442"){
                            if(decrypt($_GET['nzend'])!=''){
                            echo "
                            <h2>Estimado Ejecutivo(a):</h2>
                            <p>Esta solicitud ya ha sido ingresada y se encuentra en<br/> proceso de gestión con la compañía, el número de<br/> requerimiento es el #".decrypt($_GET['nzend']).".</p>";
                            }else{
                            echo "
                            <h2>Estimado Ejecutivo(a):</h2>
                            <p>Esta solicitud ya ha sido ingresada y se encuentra en<br/> proceso de gestión con la compañía</p>";    
                            }
                        }elseif($error == "3539"){
                            echo '<h2>Error Token</h2>';
                        }elseif($error == "8383"){
                            echo '<div>
                        <p>El cliente no se encuentra registrado aún en esta Plataforma de Seguros. Por favor, envía un correo a tu <strong>Ejecutiva Comercial de Seguros</strong> con los siguientes datos de tu cliente para que sea ingresado en máximo <strong>48 horas</strong>:
                    </p>
                    <br>
                    
                    <ul style="text-align: left;">
                        <li style="margin-bottom: 5px;">Nombre completo*</li>
                        <li style="margin-bottom: 5px;">Rut*</li>
                        <li style="margin-bottom: 5px;">Teléfono*</li>
                        <li style="margin-bottom: 5px;">Mail*</li>
                        <li style="margin-bottom: 5px;">Fecha de nacimiento*</li>
                    </ul>
                    
                    <p style="text-align: left;">*Datos obligatorios</p>
                    
                    <p style="text-align: left;"><strong>Coordinadoras de Seguros según tu zona:</strong></p>

                    <table style="border-spacing: 1px;">
                          <tr>
                            <th style="border: solid 1px #d2d2d2; width: 15%;">Zona</th>
                            <th style="width: 35%; border: solid 1px #d2d2d2">Ejecutiva Comercial</th>
                            <th style="width: 30%; border: solid 1px #d2d2d2">División</th>
                            <th style="width: 25%; border: solid 1px #d2d2d2">Sub División</th>
                          </tr>
                          <tr>
                            <td style="border: solid 1px #d2d2d2"><strong>I</strong></td>
                            <td style="border: solid 1px #d2d2d2; text-align: left; padding: 10px;">
                                <strong>Mónica Fuentes</strong><br>
                                monica.fuentes@itau.cl<br>
                                Anexo:0348
                            </td>
                            <td style="border: solid 1px #d2d2d2;">Santiago Sur <br>Santiago Centro</td>
                            <td style="border: solid 1px #d2d2d2;"> - </td>
                          </tr>
                          <tr>
                            <td style="border: solid 1px #d2d2d2"><strong>II</strong></td>
                            <td style="border: solid 1px #d2d2d2; text-align: left; padding: 10px;">
                                <strong>Makarena Vargas</strong> <br>
                                makarena.vargas@itau.cl <br>
                                Anexo:7220
                            </td>
                            <td style="border: solid 1px #d2d2d2">Santiago Oriente <br>Personal Bank</td>
                            <td style="border: solid 1px #d2d2d2"> - </td>
                          </tr>
                          <tr >
                            <td style="border: solid 1px #d2d2d2" ><strong>III</strong></td>
                            <td style="border: solid 1px #d2d2d2; text-align: left; padding: 10px;">
                                <strong>Marjorie Silva</strong><br>
                                marjorie.silva@itau.cl<br>
                                Anexo:7221
                            </td>
                            <td style="border: solid 1px #d2d2d2; padding: 10px;">
                                Santiago Cordillera<br>
                                Región VIII<br>
                                Región Centro<br>
                                Región Sur

                            </td>
                            <td style="border: solid 1px #d2d2d2">Rancagua<br>San Fernando</td>
                          </tr>
                          <tr>
                            <td style="border: solid 1px #d2d2d2"><strong>IV</strong></td>
                            <td style="border: solid 1px #d2d2d2; text-align: left; padding: 10px;">
                                <strong>Carolina Montoya</strong><br>
                                carolina.montoya@itau.cl<br>
                                Anexo:7219
                            </td>
                            <td style="border: solid 1px #d2d2d2; padding: 10px;">
                                Santiago Poniente<br>
                                Región V<br>
                                Región Norte<br>
                                Región Centro

                            </td>
                            <td style="border: solid 1px #d2d2d2">Curic&oacute;<br>Talca</td>
                          </tr>

                        </table>
                    </div>';
                        }

                    ?>

                </div>    
            </div>
        </div>
    </div>




</body>
</html>