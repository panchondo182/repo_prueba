<?php
session_start();
	if(isset($_SESSION['sessInit']) && isset($_SESSION['sesstkn']))
	{
		//SI ES ASI REDIRIGIR A INICIO
		header('Location: bypass.php');
	}
	/*	
	if(isset($_SESSION['sessInit']) && isset($_SESSION['sesstkn']))
	{
		header('Location: index.php');
	}
	else
	{
		echo "datos: ".session_id()."<br>";
		echo "tkn: ".$_SESSION['sesstkn']."<br>";
	}

	$idTokenString = $_SESSION['sesstkn'];
	$result = FRB::getInstance()->verifyGetToken($idTokenString);
	
	if($result==null)
	{
		unset ($_SESSION['sessInit']);
		unset ($_SESSION['sesstkn']);
		header('Location: login.php');
	}*/
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
    <meta http-equiv= "X-UA-Compatible" content="IE=9"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Itau</title>
	<link rel="stylesheet" type="text/css" href="css/CSS-LGN.css">
	<script src="https://apis.google.com/js/api.js"></script>		
</head>

<body class="bg">
	<div id="nav-fixed">
		<img class="nav-brand" src="img/logo.png"></img>
		
	</div>
	<div id="content">
		<div class="row formLgn">
			<div class="col-12 frmPanel">
				<div class="row frmLabel">
					<img class="nav-brand-2" src="img/logo-alwayson.png"></img>
				</div>
				<div class="row">
					<label class="col-5" for ="frmLgnUS" >USUARIO</label>	
					<input autocomplete="true" class="col-7 frmInp" type="text" name="frmLgnUS" id="frmLgnUS" placeholder="email"/>
				</div>
				
				<!--<input type="hidden" name="AO_Cargo" id="AO_Cargo" value="ANALISTA">-->
				<div class="row">
					<label class="col-5" for ="frmLgnPW" >CONTRASEÑA</label>	
					<input class="col-7 frmInp" type="PASSWORD" name="frmLgnPW" id="frmLgnPW" placeholder="********" maxlength="20"/>
				</div>
				<div class="row">
					<div class="col-12">
						<button class="btn btn-login" id="btnlogin">Iniciar Sesion</button>
					</div>
				</div>
			</div>	
		</div>
	</div>

    
	<div id="ban_bottom">
		
	</div>
	<script src="js/jquery-3.3.1.js" ></script>
	<!-- FIREBASE -->
	<!-- LIMITAMOS A CARGAR SOLO LA BASE DE FIREBASE-->
	<script src="https://www.gstatic.com/firebasejs/5.4.1/firebase-app.js"></script>
	<!-- CARGAMOS EL MODULO DE AUTENTIFICACION-->
	<script src="https://www.gstatic.com/firebasejs/5.0.0/firebase-auth.js"></script>
	<!-- PASAMOS LA CONFIGURACION E INICIALIZAMOS FIREBASE-->
	<script>
	  // CONFIGURACION DE INICIALIZACION DE FIREBASE CLIENT WEB
	  var config = {
		apiKey: "AIzaSyCg5fY6ElkTAfJ0yED9cIR8XmGS3yx43c4",
		authDomain: "frb-seg.firebaseapp.com",
		databaseURL: "https://frb-seg.firebaseio.com",
		projectId: "frb-seg",
		storageBucket: "",
		messagingSenderId: "531664130608"
	  };
	  firebase.initializeApp(config);
	</script>

	<script>
		//OBSERVADOR DE FIREBASE SE PROPAGA UNA VEZ SETEADO
		// ESTE OBSERVADOR DETECTA LOS CAMBIOS EN LA SESION LIMITADO A SIGN IN | SIGN OUT
		// EN CASO DE GENERAR ALGUNA DE ESTAS ACCIONES SE ACTIVARA IGUALMENTE ENTREGARA UN MENSAJE EN CONSOLA AL SETEARSE
		firebase.auth().onAuthStateChanged(function(user) {
		if (user) 
		{
			//console.log(user);
			//login(); //no usar puede generar un ciclo infinito
			// User is signed in.
		}
		else
		{
			//logout(); //no usar puede generar un ciclo infinito
			//console.error('error usuario esta en estado: ',user);
		}
		});

	</script>
<!--//-->
<!-- LOGIN -->
	<script src="js/fs_log.js"></script>
<!--//-->
</body>

</html>