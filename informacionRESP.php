<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="UTF-8">
    <meta http-equiv= "X-UA-Compatible" content="IE=9"/>
    <title>Portal de Seguros</title>
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">
   
    <?php
    require_once 'class/config.php';
    require_once 'class/generales_class.php';
    require_once 'class/generales_validacionesCliente.php';
	session_start(); //iniciamos el manejo de sesiones

    $_SESSION['rut_cliente2'] = $_SESSION['rut'];			
		
    $data = isset($_GET["data"]) ? $_GET["data"] : '';  
    $rut=decrypt($data);    

    $cargo = isset($_SESSION['cargo']) ? $_SESSION['cargo'] : ''; 
    
    switch($cargo){
        case 'BTEL':
            header("location: listaSeguros.php?data=".$data);
            break;
        case '':
            session_destroy();
            header("location: mensajeError.php?codigo=3535");
            break;
    } 


    if($data!=''){        
        include('class/datosCliente.php');
    }else{
        header("location: index.php");
    }
    ?>
</head>
<body>
<script type="text/javascript">
function cerrar() {
	//alert("aaaa");
ventana=window.self;
ventana.opener=window.self;
ventana.close(); 
}
</script>
    <header>
        <div class="logo_itau">
           <img src="assets/img/logo-itau.png" alt="">

        </div>
        <div class="header_right">
            Plataforma Comercial 
        </div>
    </header>
    <div class="clear"></div>
    <div class="container">
        <?php include('menuLateral.php');?>
        <div class="modulo">
            <nav>
                <ul class="tabs">
                    <li class="tabs_active margen-bottom-10"><a href="#" id="tab1">Información del cliente</a></li>
                    <li class="margen-bottom-10"><a href="segurosContratados.php?data=<?php echo $data; ?>" id="tab2">Seguros contratados</a></li>
					<li class="margen-bottom-10"><a href="postventa.php?data=<?php echo $data; ?>" id="">Post-venta</a></li>
                      
                </ul>
            </nav>
            <div class="contenido">
                <div class="info_cliente">
                    <table class="table_left marginr2">
                        <tr>
                            <td><h1><?php echo $datos['nom']." ".$datos['ape']; ?></h1></td>
                        </tr>
                        <tr>
                            <td><?php echo comun_formatoPuntoRut($rut); ?></td>
                        </tr>
                        <tr>
                            <td>No Informado</td>
                        </tr>
                    </table>
                    <table class="table_middle">
                        <tr>
                            <td><strong>Edad:</strong> <?php if($datos['fna']=='') echo "No Informado"; else echo $datos["edad"]." Años"; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Profesión:</strong> No Informado</td>
                        </tr>
                        <tr>
                            <td><strong>Trabajador:</strong> No Informado</td>
                        </tr>
                        <tr>
                            <td><strong>Empresa:</strong> No Informado</td>
                        </tr>						
                        <tr>
                            <td><strong>Hijos:</strong> No Informado</td>
                        </tr>
                    </table>
                    <table class="table_right">
                        <tr>
                            <td><strong>Viajes:</strong> No Informado</td>
                        </tr>
                        <tr>
                            <td><strong>Email:</strong> <?php if($datos['mail']=='') echo "No Informado"; else echo $datos['mail']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Tel. Personal:</strong> <?php if($datos['tel4']=='') echo "No Informado"; else echo $datos['tel4']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Tel. Comercial:</strong> <?php if($datos['tel3']=='') echo "No Informado"; else echo $datos['tel3']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Ejecutivo:</strong> <?php echo $_SESSION['apenom']; ?></td>
                        </tr>
                    </table>            
                </div>
                <div class="seguros_recomendados">
                    <h3 class="margintb">Seguros recomendados</h3>
                    <div class="modulo_seguros">
                        <div class="titulo_seguro">
                            <i class="i_vida"></i>Vida
                        </div>
                        <div class="box_seguro">
                            <ul class="bullet">
                                <li><a href="productos/SVBBSS017/informacion.php?data=<?php echo $data;?>" class="underline">Vida con Bonificación por Permanencia Clásico</a></li>
                            </ul>
                        </div>  
                    </div>
                    <div class="modulo_seguros">
                        <div class="titulo_seguro">
                            <i class="i_proteccion"></i>Protección
                        </div>
                        <div class="box_seguro">
                             <ul class="bullet">
                                <li><a href="productos/SVBBSS019/informacion.php?data=<?php echo $data; ?>" class="underline">Protección Tradicional</a></li>
                                <!-- <li>Desgravamen + ITO 2/3 individual</li> -->
                            </ul>
                        </div>  
                    </div>
                    <div class="modulo_seguros">
                        <div class="titulo_seguro">
                            <i class="i_asistencias"></i>Asistencias
                        </div>
                        <div class="box_seguro">
                            <ul class="bullet">
                                <li><a href="productos/SVBBSS026/informacion.php?data=<?php echo $data; ?>" class="underline">Viaje Protegido Plus</a></li>
                            </ul>
                        </div>  
                    </div>
                    <div class="modulo_seguros">
                        <div class="titulo_seguro">
                            <i class="i_hogar"></i>Hogar
                        </div>
                        <div class="box_seguro">
                            <ul class="bullet">
                               <li><a href="productos/SVBBSS023/informacion.php?data=<?php echo $data; ?>" class="underline">Hogar Contenido</a></li>
                            </ul>
                        </div>  
                    </div>
                    
                    <div class="modulo_seguros">
                        <div class="titulo_seguro">
                            <i class="i_auto"></i>Automotriz
                        </div>
                        <div class="box_seguro">
                            <ul class="bullet">
                               <li><a href="productos/SVBBSS035/informacion.php?data=<?php echo $data; ?>" class="underline">Seguro Automotriz</a></li>
                            </ul>
                        </div>  
                    </div>

                    <!--<a class="btn_gris displayb margins0" href="index.php?return=true">Volver</a>  -->          
                </div>
            </div>   
        </div>
    </div>
</body>
</html>