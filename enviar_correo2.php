<?php
error_reporting(E_ALL); 
ini_set('display_errors', 1);

require_once 'class/config.php';
require_once 'class/generales_class.php';
require_once 'class/generales_validacionesCliente.php';
require_once 'class/itauEnvioCorreo.php';

$data = isset($_GET["data"]) ? $_GET["data"] : ''; 

date_default_timezone_set("America/Santiago");
$grabacion	= date("Y-m-d");
$mysqli = new mysqli($DB['host'], $DB['user'], $DB['pass'], $DB['useBI']);

$fechahoy = date('Y-m-j');

$datos_poliza = explode('*-*', $_GET['pol']);
$poliza = $datos_poliza[0];
$asociado_credito = $datos_poliza[1];
$codigo_producto = $datos_poliza[2];
$num_credito = $datos_poliza[3];
$num_poliza = $datos_poliza[4];
$flujo = $datos_poliza[5];
$idcontrato = $datos_poliza[6];


if($flujo=='V'){
//comparo numero de poliza    
$querym = "SELECT * FROM requerimientos WHERE rut='".$data."' AND poliza='".$num_poliza."' AND bloqueo=1 AND requerimiento='".$_GET['req']."' AND requerimiento in ('eliminacion_de_seguro','devolucion_de_prima','copia_de_poliza_o_certificado')";
}
if($flujo=='W'){
//comparo idcontrato
$querym = "SELECT * FROM requerimientos WHERE rut='".$data."' AND idcontrato='".$idcontrato."' AND bloqueo=1 AND requerimiento='".$_GET['req']."' AND requerimiento in ('eliminacion_de_seguro','devolucion_de_prima','copia_de_poliza_o_certificado')";    
}
$resultm = $mysqli->query($querym);
$fila = $resultm->fetch_assoc();
$nfilas = $resultm->num_rows;

//si contador es mayor a 1 quiere decir que ya se encuentra una poliza del cliente con el mismo tipo de requerimiento
if($nfilas>0){
        //echo "<script>location.href='index1.php?valor_1=".$_GET['valor_1']."&valor_2=".encrypt('error_requerimiento')."&valor_3=".$_GET['valor_3']."&req=".encrypt($celda['requerimiento'])."'</script>";
        echo "<script>window.location.replace('mensajeError.php?codigo=2442&data=".$data."&nzend=".encrypt($fila['idzendesk'])."')</script>";
        exit();
}

$data = $_GET['data'];
$datos_rut	= decrypt($_GET['data']);
$datos_rut	= formateo_rut($datos_rut);

$rutSAC = str_replace('.','',$datos_rut);
$rutSAC = str_replace('-','',$rutSAC);
$rutSAC = '00'.$rutSAC;

$dosrut = substr ($datos_rut, 0, 2);
$Tipo_de_Caso = '';
$Evento	= '';
$Ambito = '';
$tipalwayson = '';
$contadorn = '';

$devolver =0;

$req = $_GET['req'];
$mot = $_GET['mot'];
$mail = $_GET['email'];
$fono = $_GET['tel'];
$banc = $_GET['banco'];
$ctac = $_GET['cuenta'];


$sql_2="SELECT * from clientesNew WHERE rut ='$data' ";      
	        $result2=mysqli_query($mysqli, $sql_2);
	        $row = mysqli_fetch_array($result2, MYSQLI_ASSOC);
			$cliente_nombre=$row["nombre"]." ".$row["apellidos"];

if($flujo=='V'){
	
	$sql_2="SELECT * from codigos WHERE codigo ='$codigo_producto' ";      
	        $result2=mysqli_query($mysqli, $sql_2);
	        $row = mysqli_fetch_array($result2, MYSQLI_ASSOC);
			$npolcod=$row["nombre"];
	$sql_3="SELECT * from seguros WHERE cod_producto='$codigo_producto' and rut='$data' and poliza='$num_poliza' LIMIT 1";
			$result3=mysqli_query($mysqli, $sql_3);
			$row3 = mysqli_fetch_array($result3, MYSQLI_ASSOC);
			$npolseg=$row3["nom_producto"];

			if($npolcod=='' || $npolcod=='No tiene'){
				$npol = $npolseg;
			}else{
				$npol = $npolcod;
			}	
}
if($flujo=='W'){
	switch($codigo_producto){
		case "SVBBSS019":
			$npol='Protecci&oacute;n Tradicional';
			break;
		case "SVBBSS017":
			$npol='Vida con Bonificaci&oacute;n';
			break;
		case "SVBBSS023":
			$npol='Hogar Contenido';
			break;
		case "SVBBSS026":
			$npol='Viaje Pretegido Plus';
			break;
		case "SVBBSS030":
			$npol='Protecci&oacute;n Preferente';
			break;
		case "SVBBSS035":
			$npol='Seguro Automotriz';
			break;
	}
	
 }

if($_GET['req']=='copia_de_poliza_o_certificado'){
    $nuevafecha = strtotime('+5 day', strtotime($fechahoy));
}
if($_GET['req']=='eliminacion_de_seguro'){
    $nuevafecha = strtotime('+10 day', strtotime($fechahoy));
}
if($_GET['req']=='devolucion_de_prima'){
    $nuevafecha = strtotime('+11 day', strtotime($fechahoy));
}

$nuevafecha = date('Y-m-j',$nuevafecha);

if($contadorn==0){
$query="insert into bancoitau.requerimientos (rut,nombrecli,codigoseg,nombreseg,tipocre,numcre,poliza,requerimiento,motivo,fechaingreso,email,telefono,banco,ctacte,estado,flujo,idcontrato,bloqueo,sla_new)";
$query.="values('$data','$cliente_nombre','$codigo_producto','$poliza','$asociado_credito','$num_credito','$num_poliza','$req','$mot','$grabacion','$mail','$fono','$banc','$ctac','Pendiente de Firma','$flujo','$idcontrato','0','$nuevafecha')";
//echo $query;
$result = $mysqli->query($query);
$ultimoId = $mysqli->insert_id; 
}


if($req=='eliminacion_de_seguro'){
	$tiporeq='Eliminaci&oacute;n de seguro';
}
if($req=='devolucion_de_prima'){
	$tiporeq='Devoluci&oacute;n de prima';
}

//echo 'eso es $npol: '.$npol;

$mensaje='<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<title>Ita&uacute; Seguros</title>
			</head>
			<body style="margin: 0; font-family: Arial; background: #FFF;">
			<table width="540" align="center">
                <tr>
                   <td>
                    <td>
            <table cellpadding="0" cellspacing="0" border="0" width="540" style="background: #F3791F;">
                <tr>
                    <td height="15px">

                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="540" align="center" style="border: solid 1px #e2e6ea; background: #f7f4ef;padding: 20px; margin: 0 auto">
                <tr>
                    <td>  
                    <table>
                        <tr>
                            <td style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #595959; padding: 0 30px 20px; vertical-align: bottom" width="360">
                                '.ucwords($cliente_nombre).', 
                            </td>
                            <td style="padding-bottom: 20px">
                                <img src="https://banco.itau.cl/publicThemeStatic/themes/publicTheme/css/publico/images/logo-itau.png" alt="" width="45">
                            </td>
                        </tr>
                    </table> 
                    </td>
                </tr>
                <tr style="background: #FFF;">
                    <td align="center" valign="top" height="45"> </td>
                </tr>
                <tr style="background: #FFF;">
                    <td style="text-align: center; font-size:22px; color:#373e47; padding: 0 30px; font-family: Arial, Helvetica, sans-serif;">
                        Confirma la <strong>'.$tiporeq.'</strong>  <br> en el sitio privado
                    </td>
                </tr>
                <tr style="background: #FFF;">
                    <td align="center" valign="top" height="30"> </td>
                </tr>
                <tr style="background: #FFF">
                    <td style="color:#616161; font-size: 14px; padding: 0 30px; line-height: 20px; font-family: Arial, Helvetica, sans-serif;">
                        El requerimiento de <strong>'.$tiporeq.'</strong> de <strong>'.$poliza.'</strong> se encuentra disponible para confirmar en tu sitio privado. Ingresa en:
                    </td>          
                </tr>
                <tr style="background: #FFF;">
                    <td align="center" valign="top" height="20"> </td>
                </tr>
                <tr style="background: #FFF;">
                    <td style="color:#616161; font-size: 14px; padding: 0 30px; line-height: 20px; font-weight: bold; font-family: Arial, Helvetica, sans-serif;">
                    Sitio privado &gt; Mis Productos &gt; Seguros &gt; y haz click en el requerimiento.
                    </td>          
                </tr>
                <tr style="background: #FFF;">
                    <td align="center" valign="top" height="20"> </td>
                </tr>
                <tr style="background: #FFF;">
                    <td style="color:#616161; font-size: 14px; padding: 0 30px; line-height: 20px; font-family: Arial, Helvetica, sans-serif;">
                        Desde ahora tienes <strong>48 hrs</strong> para confirmarlo.
                    </td>          
                </tr>
                <tr style="background: #FFF;">
                    <td align="center" valign="top" height="20"> </td>
                </tr>
                <tr style="background: #FFF;">
                    <td style="color:#616161;font-size:14px;padding:0 30px;line-height:20px; font-family:Arial,Helvetica,sans-serif;">
                        Para m&aacute;s informaci&oacute;n comun&iacute;cate a:
                    </td>          
                </tr>
                <tr style="background: #FFF;">
                    <td align="center" valign="top" height="20"> </td>
                </tr>
                <tr style="background: #FFF;">

                    <td>
                        <table>
                            <tr>
                                <td align="center" style="color:#6b6b6b; font-size: 12px; padding-left: 30px;">
                                <img border="0" width="15" id="m_6692346763504116436_x0000_i1026" src="https://ci5.googleusercontent.com/proxy/XXXcFuFgj-knyUH3KbSZcKhU0nn3orwHXE5xMkq1MOjg_9XhAOGoz0Y5Mc4eljVkjs-3-c1cY8-DDz_Pr0h1Syjdi0tO5ikB=s0-d-e1-ft#http://itaucomercialqa.alwayson.cl/img/celular.png" class="CToWUd">
                                </td>
                                <td style="color:#6b6b6b; font-size:14px;padding:0 15px;line-height:20px; font-family:Arial,Helvetica,sans-serif; font-weight: bold">
                                    600 600 1200                               
                                </td>
                            </tr>
                        </table>
                    </td>          
                </tr>
                <tr style="background: #FFF;">
                    <td align="center" valign="top" height="20"> </td>
                </tr>
                <tr style="background: #FFF;">
                    <td style="color:#6b6b6b; font-size: 10px; padding: 0 30px; font-weight: bold; font-family: Arial, Helvetica, sans-serif;">
                        Este email fue generado autom&aacute;ticamente, por favor no respondas este mensaje. Ante cualquier duda, cont&aacute;ctate con tu ejecutivo de cuentas o Ita&uacute;  Corredora de Seguros.
                    </td>          
                </tr>
                <tr style="background: #FFF;">
                    <td align="center" valign="top" height="20"> </td>
                </tr>
                <tr>
                    <td height="30px"></td>
                </tr>
                <tr>
                    <td>
                        <table style="border-collapse: collapse;" width="498">

                            <tr style="background: #FFF;">
                                <td align="center" style="padding-left: 30px">
                                    <img src="https://ci6.googleusercontent.com/proxy/aP_L8N-_elPitNOmoDUAAm8La8qhTTOg9dANn0JVWQJyootM3c_0qKGCsnEg4ST4F3bKM2D9n3HeaRI6y1Udy33HNVgoS4_nkN3f2C8wCdrK=s0-d-e1-ft#http://desarrollo.alwayson.es/ITAU-Consulta/lock-e-mail.png" alt="" width="25" style="text-align:center" class="CToWUd">
                                    <p style="font-size: 10px; margin-top: 3px; color: #F3791F; font-family: Arial, Helvetica, sans-serif;">Email seguro</p>
                                </td>
                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; line-height: 15px;padding: 10px 0 5px 0;color: #6b6b6b; background:#fff">
                                    <ul style="padding-left: 25px;">
                                        <li style="color: #F3791F;"><span style="color: #595959;">Siempre escribe <b>www<font style="font-size:1px">&nbsp;</font>.itau.<font style="font-size:1px">&nbsp;</font>cl</b> en la barra del navegador.</span></li>
                                        <li style="color: #F3791F;"><span style="color: #595959;">Nunca te enviaremos correos con links.</span></li>
                                        <li style="color: #F3791F;"><span style="color: #595959;">Modifica regularmente las contrase&ntilde;as de tus tarjetas y clave de acceso.</span></li>
                                        <li style="color: #F3791F;"><span style="color: #595959;">Nunca pediremos datos personales, claves o informaci&oacute;n de coordenadas.</span></li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>    
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="540" style="background: #d0d0d0;">
                <tr>
                    <td height="15px">

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>';

    $Msubjet='Tu requerimiento está disponible para confirmar';
    $Morigen='itaucorredoresdeseguros@itau.cl';
    $Mdestino=$mail;
    //$Mdestino='vcofre@alwayson.es';
    $MCC='';
    $MCCO='';
    $MnombreAdjunto='';
    $Mcuerpo='<![CDATA['.$mensaje.']]>';
    $Madjunto='<![CDATA[]]>';
      //echo $Madjunto;
    $enviarCorreo=enviarMailPorItauSA($Msubjet,$Morigen,$Mdestino,$MCC,$MCCO,$Mcuerpo); 

  if($contadorn==0){          
    echo '<script>location.href="finalizar2.php?data='.$_GET['data'].'&pol='.encrypt($_GET['pol']).'&req='.$req.'&npol='.$npol.'"</script>';
  }else{
    echo "<script>window.location.replace('mensajeError.php?codigo=2442&data=".$data."')</script>";
  }
//}
?>