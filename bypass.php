<?php
session_start();
	if(!isset($_SESSION['sessInit']) && !isset($_SESSION['sesstkn']))
	{
		header('Location: login.php');
	}
	else
	{
		//echo "datos: ".session_id()."<br>";
		//echo "tkn: ".$_SESSION['sesstkn']."<br>";
	}
	
	/*session_start();

	require_once __DIR__.'/FirSDK.php';
	
	if(isset($_SESSION['sessInit']) && isset($_SESSION['sesstkn']))
	{
		header('Location: index.php');
	}
	else
	{
		echo "datos: ".session_id()."<br>";
		echo "tkn: ".$_SESSION['sesstkn']."<br>";
	}

	$idTokenString = $_SESSION['sesstkn'];
	$result = FRB::getInstance()->verifyGetToken($idTokenString);
	
	if($result==null)
	{
		unset ($_SESSION['sessInit']);
		unset ($_SESSION['sesstkn']);
		header('Location: login.php');
	}*/
?> 
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
    <meta http-equiv= "X-UA-Compatible" content="IE=9"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Itau</title>
	<link rel="stylesheet" type="text/css" href="css/CSS-LGN.css">		
</head>

<body class="bg">
	<div id="nav-fixed">
		<img class="nav-brand" src="img/logo.png"></img>
	</div>
	<div id="content">
		<div class="row formLgn">
			<div class="col-12">
			<div class="row frmLabel">
				<img class="nav-brand-2" src="img/logo-alwayson.png"></img>
			</div>
			<div class="col-12">
			<form method="post" action="index.php">
				
				<input type="hidden" name="Estado" id="Estado" value="000">
				<input type="hidden" name="Descripcion" id="Descripcion" value="Guardado Correctamente">
				<!--<input type="hidden" name="AO_Token"   	   	 	id="AO_Token"    	value="931cba7a0fd11383e3e644ecaa8846ca">-->
				<input type="hidden" name="AO_Token" id="AO_Token" value="V1ct0r.C0fr3">
				<!--<input type="hidden" name="AO_Token" id="AO_Token" value="5534cf3de99dda76cfb28f3918312e8c">-->
				<input type="hidden" name="AO_Incremental" id="AO_Incremental" value="1234">
				<input type="hidden" name="AO_UserID" id="AO_UserID" value="CROSAS">
				<input type="hidden" name="AO_Apenom" id="AO_Apenom" value="CRISTIAN ROSAS">
				<input type="hidden" name="AO_Suc" id="AO_Suc" value="SANTIAGO">
				<input type="hidden" name="AO_NomSuc" id="AO_NomSuc" value="SANTIAGO MATRIZ">
				
				<div class="row">
					<label class="col-4" for ="AO_rut" >RUT Cliente</label>
					<input class="col-8 frmInp" type="text" name="AO_rut" id="AO_rut" value="" placeholder="Rut Cliente" maxlength="9" required>
				</div>
				
				<!--<input type="hidden" name="AO_Cargo" id="AO_Cargo" value="ANALISTA">-->
				<div class="row">
					<label class="col-4" for ="AO_Cargo" >Perfil Ejecutivo</label>
					<select class="col-8 frmInp" name="AO_Cargo" id="AO_Cargo">
						<option value="EJECUTIVO">Ejecutivo</option>
						<option value="AD_CTACTE">Cuenta Corriente</option>
						<option value="BTEL">Banca Telef&oacute;nica</option>
						<option value="JE_OPERA">Jefe de Operaciones</option>
						<option value="AGENTE">Agente</option>
						<option value="TESORERO">Tesorero</option>
					</select>
				</div>
					<input type="hidden" name="AO_Suc" id="AO_Suc" value="SANTIAGO">
					<input type="hidden" name="AO_user" id="AO_user" value="Usuario Red ITAUCHILE">
					<input type="hidden" name="AO_UrlValidaPinItau" id="AO_UrlValidaPinItau" value="../comun/goItau.php">
				
				<!--<input type="hidden" name="AO_UrlVolver" id="AO_UrlVolver" value="http://desarrollo.alwayson.es/Desarrollo/PlataformaComercial2.6/productos/comun/backItau.php">-->
				
					<input type="hidden" name="AO_UrlVolver" id="AO_UrlVolver" value="backItau.php">      
					<input type="hidden" name="AO_serverName" id="AO_serverName" value="Usuario Red ITAUCHILE ">
				<div class="row">
					
					<div class="col-6">
						<input type="submit" class="btn btn-login" name="enviar" id="enviar" value="Ir al portal">
					</div>
					<div class="col-6">
						<button class="btn btn-login" type="button" id="btnlogout" >Cerrar Sesion</button>
					</div>
				</div>
				
				</div>
			</form>
			</div>
		</div>
	</div>
    
	<div id="ban_bottom">
		
	</div>
	<script src="js/jquery-3.3.1.js" ></script>
	<!-- FIREBASE -->
	<!-- LIMITAMOS A CARGAR SOLO LA BASE DE FIREBASE-->
	<script src="https://www.gstatic.com/firebasejs/5.4.1/firebase-app.js"></script>
	<!-- CARGAMOS EL MODULO DE AUTENTIFICACION-->
	<script src="https://www.gstatic.com/firebasejs/5.0.0/firebase-auth.js"></script>
	<!-- PASAMOS LA CONFIGURACION E INICIALIZAMOS FIREBASE-->
	<script>
	  // CONFIGURACION DE INICIALIZACION DE FIREBASE CLIENT WEB
	  var config = {
		apiKey: "AIzaSyCg5fY6ElkTAfJ0yED9cIR8XmGS3yx43c4",
		authDomain: "frb-seg.firebaseapp.com",
		databaseURL: "https://frb-seg.firebaseio.com",
		projectId: "frb-seg",
		storageBucket: "",
		messagingSenderId: "531664130608"
	  };
	  firebase.initializeApp(config);
	</script>

	<script>
		//OBSERVADOR DE FIREBASE SE PROPAGA UNA VEZ SETEADO
		// ESTE OBSERVADOR DETECTA LOS CAMBIOS EN LA SESION LIMITADO A SIGN IN | SIGN OUT
		// EN CASO DE GENERAR ALGUNA DE ESTAS ACCIONES SE ACTIVARA IGUALMENTE ENTREGARA UN MENSAJE EN CONSOLA AL SETEARSE
		firebase.auth().onAuthStateChanged(function(user) {
		if (user) 
		{
			//console.log(user);
			//login(); no usar puede generar un ciclo infinito
			// User is signed in.
		}
		else
		{
			//logout(); no usar puede generar un ciclo infinito
			//console.error('error usuario esta en estado: ',user);
		}
		});

	</script>
<!--//-->
<!-- LOGIN -->
	<script src="js/fs_log.js"></script>
<!--//-->
</body>

</html>