<?php 

require_once 'class/itauEnvioCorreo.php';
session_start();
    //echo $DB['host']." ". $DB['user']." ". $DB['pass']." ". $DB['useBI'];
$data = isset($_GET["data"]) ? $_GET["data"] : ''; 
$cargo = isset($_SESSION['cargo']) ? $_SESSION['cargo'] : '';
$compania_corto = isset($_GET['comp']) ? $_GET['comp'] : '';
$cliente_nombre = isset($_GET['nomcli']) ? $_GET['nomcli'] : '';
$nombre_pdf = isset($_GET['nombre']) ? $_GET['nombre'] : '';
$mail = isset($_GET['mail']) ? $_GET['mail'] : ''; 
$req = isset($_GET["req"]) ? $_GET["req"] : ''; 
$idzen = isset($_GET["idzen"]) ? $_GET["idzen"] : ''; 
$num_credito = isset($_GET["num_credito"]) ? $_GET["num_credito"] : ''; 
switch($req){
	case 'copia_de_poliza_o_certificado':
        $reque='Copia de p&oacute;liza';
        break;
	case 'eliminacion_de_seguro':
        $reque='Eliminar seguro';
        break;
	case 'devolucion_de_prima':
        $reque='Devoluci&oacute;n de prima';
        break;
}
$npol = isset($_GET["npol"]) ? $_GET["npol"] : ''; 

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Portal de Seguros</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9"/>
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/css/postventa.css" rel="stylesheet" >
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="assets/js/jquery-function.js"></script>
    <script src="assets/js/modal.js" type="text/javascript"></script>
    <script>
        function enviar_formulario(){
            document.formulario1.submit()
        }
    </script> 
    
<?php
    
  if(($compania_corto=='sura' or $compania_corto=='SURA') and $_GET['nombre']!='NN' and $num_credito==''){
	  while(!file_exists("pdf/".$nombre_pdf)){
		  sleep(5);
	  }
$mensaje= '<table width="540" align="center">
                <tbody><tr>
                   <td>
                    </td><td>
            <table cellpadding="0" cellspacing="0" border="0" width="540" style="background:#f3791f">
                <tbody>
                    <tr>
                        <td height="15px">
                        </td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="540" align="center" style="border:solid 1px #e2e6ea;background:#f7f4ef;padding:20px;margin:0 auto">
                <tbody>
                    <tr>
                        <td>  
                            <table>
                                <tbody>
                                    <tr>
                                        <td style="font-family:Arial,Helvetica,sans-serif;font-size:14px;color:#595959;padding:0 30px 20px;vertical-align:bottom" width="360">
                                            '.$cliente_nombre.',
                                        </td>
                                        <td style="padding-bottom:20px">
                                            <img src="https://banco.itau.cl/publicThemeStatic/themes/publicTheme/css/publico/images/logo-itau.png" alt="" width="45" class="CToWUd">
                                        </td>
                                    </tr>
                                </tbody>
                            </table> 
                        </td>
                    </tr>
                    <tr style="background: #FFF;">
                        <td align="center" valign="top" height="45"> </td>
                    </tr>
                    <tr style="background: #FFF;">
                        <td style="text-align: center; font-size:22px; color:#373e47; padding: 0 30px; font-family: Arial, Helvetica, sans-serif;">
                            &#161;Tu requerimiento ha finalizado exitosamente&#33;
                        </td>
                    </tr>

                    <tr style="background:#fff">
                        <td align="center" valign="top" height="30"> </td>
                    </tr>
                    <tr style="background:#fff">
                        <td style="font-family:Arial,Helvetica,sans-serif; color:#616161; font-size:14px;padding:0 30px;line-height:20px;">
                           Adjunto encontrar&aacute;s la copia de p&oacute;liza de tu seguro <strong>'.$nom_seguro.'</strong>. 
                        </td>          
                    </tr>
                    <tr style="background:#fff">
                        <td align="center" valign="top" height="20"> </td>
                    </tr>
                    <tr style="background:#fff">
                        <td style="color:#616161;font-size:14px;padding:0 30px;line-height:20px; font-family:Arial,Helvetica,sans-serif;">
                            Para m&aacute;s informaci&oacute;n comun&iacute;cate a: 
                        </td>          
                    </tr>
                    <tr style="background:#fff">
                        <td align="center" valign="top" height="10"> </td>
                    </tr>
                    <tr style="background:#fff">
                        <td>
                            <table>
                                <tbody>
                                <tr>
                                    <td align="center" style="color:#6b6b6b;font-size:12px;padding-left:30px">
                                    <img border="0" width="15" id="m_6692346763504116436_x0000_i1026" src="https://ci5.googleusercontent.com/proxy/XXXcFuFgj-knyUH3KbSZcKhU0nn3orwHXE5xMkq1MOjg_9XhAOGoz0Y5Mc4eljVkjs-3-c1cY8-DDz_Pr0h1Syjdi0tO5ikB=s0-d-e1-ft#http://itaucomercialqa.alwayson.cl/img/celular.png" class="CToWUd">
                                    </td>
                                    <td style="color:#6b6b6b; font-size:14px;padding:0 15px;line-height:20px; font-family:Arial,Helvetica,sans-serif;">
                                        <a href="tel:+56%202%202686%200999" value="+56226860999" target="_blank" style="text-decoration: none; color: #6b6b6b;"><strong>600 600 1200</strong></a> 
                                    </td>

                                </tr>
                            </tbody></table>
                        </td>          
                    </tr>
                    <tr style="background:#fff">
                        <td align="center" valign="top" height="20"> </td>
                    </tr>
                    <tr style="background:#fff">
                        <td style="color:#6b6b6b;font-size:10px;padding:0 30px;font-weight:bold; font-family:Arial,Helvetica,sans-serif;">
                            Este email fue generado autom&aacute;ticamente, por favor no respondas este mensaje. Ante cualquier duda, cont&aacute;ctate con tu ejecutivo de cuentas o Ita&uacute; Corredora de Seguros.
                        </td>          
                    </tr>
                    <tr style="background:#fff">
                        <td align="center" valign="top" height="20"> </td>
                    </tr>
                    <tr>
                        <td height="30px"></td>
                    </tr>

                    <tr>
                        <td>
                            <table style="border-collapse:collapse" width="498">

                                <tbody><tr style="background:#fff">
                                    <td align="center" style="padding-left:30px">
                                        <img src="https://ci6.googleusercontent.com/proxy/aP_L8N-_elPitNOmoDUAAm8La8qhTTOg9dANn0JVWQJyootM3c_0qKGCsnEg4ST4F3bKM2D9n3HeaRI6y1Udy33HNVgoS4_nkN3f2C8wCdrK=s0-d-e1-ft#http://desarrollo.alwayson.es/ITAU-Consulta/lock-e-mail.png" alt="" width="25" style="text-align:center" class="CToWUd">
                                        <p style="font-size:10px;margin-top:3px;color:#f3791f; font-family:Arial,Helvetica,sans-serif;">Email seguro</p>
                                    </td>
                                    <td style="font-family:Arial,Helvetica,sans-serif;font-size:10px;line-height:15px;padding:10px 0 5px 0;color:#6b6b6b;background:#fff">
                                            <ul style="padding-left:25px">
                                                <li style="color:#f3791f"><span style="color:#595959">Siempre escribe <a href="http://www.itau.cl" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=es&amp;q=http://www.itau.cl&amp;source=gmail&amp;ust=1513080086695000&amp;usg=AFQjCNGSO6XTcNiOG96XtOKT_Vq5tYRX2Q" style="color:#6b6b6b; text-decoration: none;"><b>www<font style="font-size:1px">&nbsp;</font>.itau.<font style="font-size:1px">&nbsp;</font>cl</b></a> en la barra del navegador.</span></li>
                                                <li style="color:#f3791f"><span style="color:#595959">Nunca te enviaremos correos con links.</span></li>
                                                <li style="color:#f3791f"><span style="color:#595959">Modifica regularmente las contrase&ntilde;as de tus tarjetas y clave de acceso.</span></li>
                                                <li style="color:#f3791f"><span style="color:#595959">Nunca pediremos datos personales, claves o informaci&oacute;n de coordenadas.</span></li>
                                            </ul>
                                    </td>
                                </tr>
                            </tbody></table>

                        </td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="540" style="background:#d0d0d0">
                <tbody><tr>
                    <td height="15px">
                    </td>
                </tr>
            </tbody>
            </table>
            
        </td>
    </tr>
</tbody>
</table>';
	$file = "pdf/".$nombre_pdf;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    $content2 = chunk_split(base64_encode($content));


    $Msubjet='Tu copia de póliza del seguro '.$nom_seguro.' ha sido enviada con éxito';
    $Morigen='itaucorredoresdeseguros@itau.cl';
    $Mdestino=$mail;
    //$Mdestino='vcofre@alwayson.es';
    $MCC='';
    $MCCO='';
    $MnombreAdjunto=$nombre_pdf;
    $Mcuerpo='<![CDATA['.$mensaje.']]>';
    $Madjunto='<![CDATA['.$content2.']]>';
      //echo $Madjunto;
    $enviarCorreo=enviarMailPorItau($Msubjet,$Morigen,$Mdestino,$MCC,$MCCO,$Mcuerpo,$Madjunto,$MnombreAdjunto);  
      //echo $Msubjet."<br>".$Morigen."<br>".$Mdestino."<br>".$MCC."<br>".$MCCO."<br>".$Mcuerpo."<br>".$MnombreAdjunto;
  }
  if(($compania_corto=='itau' or $compania_corto=='ITAU') and $_GET['nombre']!='NN'){
	  while(!file_exists("pdf/".$nombre_pdf)){
		  sleep(5);
	  }
$mensaje= '<table width="540" align="center">
                <tbody><tr>
                   <td>
                    </td><td>
            <table cellpadding="0" cellspacing="0" border="0" width="540" style="background:#f3791f">
                <tbody>
                    <tr>
                        <td height="15px">
                        </td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="540" align="center" style="border:solid 1px #e2e6ea;background:#f7f4ef;padding:20px;margin:0 auto">
                <tbody>
                    <tr>
                        <td>  
                            <table>
                                <tbody>
                                    <tr>
                                        <td style="font-family:Arial,Helvetica,sans-serif;font-size:14px;color:#595959;padding:0 30px 20px;vertical-align:bottom" width="360">
                                            '.$cliente_nombre.',
                                        </td>
                                        <td style="padding-bottom:20px">
                                            <img src="https://banco.itau.cl/publicThemeStatic/themes/publicTheme/css/publico/images/logo-itau.png" alt="" width="45" class="CToWUd">
                                        </td>
                                    </tr>
                                </tbody>
                            </table> 
                        </td>
                    </tr>
                    <tr style="background: #FFF;">
                        <td align="center" valign="top" height="45"> </td>
                    </tr>
                    <tr style="background: #FFF;">
                        <td style="text-align: center; font-size:22px; color:#373e47; padding: 0 30px; font-family: Arial, Helvetica, sans-serif;">
                            &#161;Tu requerimiento ha finalizado exitosamente&#33;
                        </td>
                    </tr>

                    <tr style="background:#fff">
                        <td align="center" valign="top" height="30"> </td>
                    </tr>
                    <tr style="background:#fff">
                        <td style="font-family:Arial,Helvetica,sans-serif; color:#616161; font-size:14px;padding:0 30px;line-height:20px;">
                           Adjunto encontrar&aacute;s la copia de p&oacute;liza de tu seguro <strong>'.$nom_seguro.'</strong>. 
                        </td>          
                    </tr>
                    <tr style="background:#fff">
                        <td align="center" valign="top" height="20"> </td>
                    </tr>
                    <tr style="background:#fff">
                        <td style="color:#616161;font-size:14px;padding:0 30px;line-height:20px; font-family:Arial,Helvetica,sans-serif;">
                            Para m&aacute;s informaci&oacute;n comun&iacute;cate a: 
                        </td>          
                    </tr>
                    <tr style="background:#fff">
                        <td align="center" valign="top" height="10"> </td>
                    </tr>
                    <tr style="background:#fff">
                        <td>
                            <table>
                                <tbody>
                                <tr>
                                    <td align="center" style="color:#6b6b6b;font-size:12px;padding-left:30px">
                                    <img border="0" width="15" id="m_6692346763504116436_x0000_i1026" src="https://ci5.googleusercontent.com/proxy/XXXcFuFgj-knyUH3KbSZcKhU0nn3orwHXE5xMkq1MOjg_9XhAOGoz0Y5Mc4eljVkjs-3-c1cY8-DDz_Pr0h1Syjdi0tO5ikB=s0-d-e1-ft#http://itaucomercialqa.alwayson.cl/img/celular.png" class="CToWUd">
                                    </td>
                                    <td style="color:#6b6b6b; font-size:14px;padding:0 15px;line-height:20px; font-family:Arial,Helvetica,sans-serif;">
                                        <a href="tel:+56%202%202686%200999" value="+56226860999" target="_blank" style="text-decoration: none; color: #6b6b6b;"><strong>600 600 1200</strong></a> 
                                    </td>

                                </tr>
                            </tbody></table>
                        </td>          
                    </tr>
                    <tr style="background:#fff">
                        <td align="center" valign="top" height="20"> </td>
                    </tr>
                    <tr style="background:#fff">
                        <td style="color:#6b6b6b;font-size:10px;padding:0 30px;font-weight:bold; font-family:Arial,Helvetica,sans-serif;">
                            Este email fue generado autom&aacute;ticamente, por favor no respondas este mensaje. Ante cualquier duda, cont&aacute;ctate con tu ejecutivo de cuentas o Ita&uacute; Corredora de Seguros.
                        </td>          
                    </tr>
                    <tr style="background:#fff">
                        <td align="center" valign="top" height="20"> </td>
                    </tr>
                    <tr>
                        <td height="30px"></td>
                    </tr>

                    <tr>
                        <td>
                            <table style="border-collapse:collapse" width="498">

                                <tbody><tr style="background:#fff">
                                    <td align="center" style="padding-left:30px">
                                        <img src="https://ci6.googleusercontent.com/proxy/aP_L8N-_elPitNOmoDUAAm8La8qhTTOg9dANn0JVWQJyootM3c_0qKGCsnEg4ST4F3bKM2D9n3HeaRI6y1Udy33HNVgoS4_nkN3f2C8wCdrK=s0-d-e1-ft#http://desarrollo.alwayson.es/ITAU-Consulta/lock-e-mail.png" alt="" width="25" style="text-align:center" class="CToWUd">
                                        <p style="font-size:10px;margin-top:3px;color:#f3791f; font-family:Arial,Helvetica,sans-serif;">Email seguro</p>
                                    </td>
                                    <td style="font-family:Arial,Helvetica,sans-serif;font-size:10px;line-height:15px;padding:10px 0 5px 0;color:#6b6b6b;background:#fff">
                                            <ul style="padding-left:25px">
                                                <li style="color:#f3791f"><span style="color:#595959">Siempre escribe <a href="http://www.itau.cl" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=es&amp;q=http://www.itau.cl&amp;source=gmail&amp;ust=1513080086695000&amp;usg=AFQjCNGSO6XTcNiOG96XtOKT_Vq5tYRX2Q" style="color:#6b6b6b; text-decoration: none;"><b>www<font style="font-size:1px">&nbsp;</font>.itau.<font style="font-size:1px">&nbsp;</font>cl</b></a> en la barra del navegador.</span></li>
                                                <li style="color:#f3791f"><span style="color:#595959">Nunca te enviaremos correos con links.</span></li>
                                                <li style="color:#f3791f"><span style="color:#595959">Modifica regularmente las contrase&ntilde;as de tus tarjetas y clave de acceso.</span></li>
                                                <li style="color:#f3791f"><span style="color:#595959">Nunca pediremos datos personales, claves o informaci&oacute;n de coordenadas.</span></li>
                                            </ul>
                                    </td>
                                </tr>
                            </tbody></table>

                        </td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="540" style="background:#d0d0d0">
                <tbody><tr>
                    <td height="15px">
                    </td>
                </tr>
            </tbody>
            </table>
            
        </td>
    </tr>
</tbody>
</table>';
	$file = "pdf/".$nombre_pdf;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    $content2 = chunk_split(base64_encode($content));


    $Msubjet='Tu copia de póliza del seguro '.$nom_seguro.' ha sido enviada con éxito';
    $Morigen='itaucorredoresdeseguros@itau.cl';
    $Mdestino=$mail;
    //$Mdestino='vcofre@alwayson.es';
    $MCC='';
    $MCCO='';
    $MnombreAdjunto=$nombre_pdf;
    $Mcuerpo='<![CDATA['.$mensaje.']]>';
    $Madjunto='<![CDATA['.$content2.']]>';
      //echo $Madjunto;
    $enviarCorreo=enviarMailPorItau($Msubjet,$Morigen,$Mdestino,$MCC,$MCCO,$Mcuerpo,$Madjunto,$MnombreAdjunto);  
      //echo $Msubjet."<br>".$Morigen."<br>".$Mdestino."<br>".$MCC."<br>".$MCCO."<br>".$Mcuerpo."<br>".$MnombreAdjunto;
  }
?>
</head>
<body>
    <header>
        <div class="logo_itau">
           <img src="assets/img/logo-itau.png" alt="">

        </div>
        <div class="header_right">
            Plataforma Comercial
        </div>
    </header>
    <div class="container_menu">
            <div class="menu">
                <div class="top_menu">
                    <h4>Portal de Seguros</h4>
                </div>
                <div class="menu_box">
                    <ul>
                        <?php if($cargo!='BTEL'){ ?><li class="marginb1"><a href="informacion.php?data=<?php echo $data; ?>">Información del cliente</a></li>                         
						<li class="marginb1"><a href="segurosContratados.php?data=<?php echo $data;?>">Seguros contratados</a></li>
                        <li><a href="postventa.php?data=<?php echo $data; ?>"><strong class="menu_active">Post-venta</strong></a></li>
						<?php }else { ?>
						<li class="marginb1"><a href="listaSeguros.php?data=<?php echo $data;?>">Seguros contratados</a></li>
                        <li><a href="bpostventa.php?data=<?php echo $data; ?>"><strong class="menu_active">Post-venta</strong></a></li>
						<?php } ?>
                    </ul>
                </div>
            </div>
        </div> 
        <div class="container-datos-datos">
        <div class="moduloPostVenta">
            <div class="titulo">
                <h4 class="blanco">
                <?php 
                if($_GET['nombre']=='NN'){
                    echo 'Enviar requrimiento'; 
                }else{
                    echo $reque; 
                }?>
                </h4>
            </div>
            <div class="contenido">
                     <div>
    					 <div class="message margint3">
                            <img src="assets/img/requerimiento.png" class="margins0">
                            <p class="text_center margintb">
							 <?php
                            if($_GET['nombre']!='NN'){ ?>
                            La copia de póliza del seguro <strong><?php echo $npol;?></strong> ha sido enviada satisfactoriamente.                     
                            <?php }?>

                            <?php 
                            if($req=='devolucion_de_prima' || $req=='eliminacion_de_seguro' || $_GET['nombre']=='NN'){?>
                                    <?php //if($_GET['nombre']=='NN'){?>
									<strong>El requerimiento nº <?php echo$idzen; ?> ha sido ingresado satisfactoriamente.</strong>
									<div>Se ha enviado un correo a <?php echo $mail; ?> con información de la solicitud:</div>
                                     <ul class="bullet">
                                            <li><strong>Contratante : </strong><?php echo $_GET['nomcli'];?></li>
                                            <li><strong>Seguro: </strong><?php echo $npol;?></li>
                                            <li><strong>Requerimiento: </strong><?php echo $reque;?></li>
                                            <li><strong>Plazo de gestión: </strong>5 días hábiles</li>
                                        </ul>                             
                                 <?php //}?>
                            <?php }?>

							</p>
                            <a class="btn_naranja margins0 displayb" href="postventa.php?data=<?php echo $data;?>" style="margin-top:10px;">Ir a Post-venta</a>
                        </div>

                </div>
 
              </div>
          </div>           
    </div>  
</body>
</html>