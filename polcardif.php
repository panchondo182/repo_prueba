<?php
function polcardif($rut, $poliza, $nom_seguro, $cliente_nombre, $mail){
	$rut. " ". $poliza. "ssss";
	$rut_busqueda	= $rut;
	$numpol = $poliza;
	$rutdec = decrypt($rut_busqueda);
	$largo = strlen($rutdec);

	$digito = substr($rutdec,$largo-1, 1);
	$rutdec = substr($rutdec,0,$largo-1);
	$rutdec = '000000000'.$rutdec;
	$rutdec = substr($rutdec, -9);

	/**************************************************************************/

	$rut=$rutdec;
	$propuesta=ltrim($numpol,'0');
	$envelope = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:v1="http://cardif.cl/Schema/EBM/IndividualizedPolicyDocument/get/v1.0" xmlns:v11="http://cardif.cl/Schema/ESO/MessageHeader/v1.0" xmlns:v12="http://cardif.cl/Schema/EBO/CompanyID/v1.0" xmlns:v13="http://cardif.cl/Schema/EBO/RUT/v1.0">
	   <soap:Header>
		  <wsse:Security soap:mustUnderstand="true" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
			 <wsse:UsernameToken wsu:Id="UsernameToken-3626398EF6DECEDA7814618725459912">
				<wsse:Username>svc_cl_Itauweb</wsse:Username>
				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">Services.Ita2017</wsse:Password>
				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">lMLo/5VopUTabJSrRj40MQ==</wsse:Nonce>
				<wsu:Created>2016-04-28T19:42:25.991Z</wsu:Created>
			 </wsse:UsernameToken>
		  </wsse:Security>
	   </soap:Header>
	   <soap:Body>
		  <v1:get_IndividualizedPolicyDocument_REQ>
			 <v11:RequestHeader>
				<v11:Consumer code="1" name="Cardif"/>
				<v11:Trace/>
				<v11:Country code="1" name="LAM"/>
				<v11:Channel code="NI" mode="NI"/>
			 </v11:RequestHeader>
			 <v1:Body>
				<v12:CompanyID>
				   <v12:companyID>0</v12:companyID>
				</v12:CompanyID>
				<v1:GetIndividualizedPolicyDocument>
				   <v1:proposalNumber>'.$propuesta.'</v1:proposalNumber>
				   <v1:insuredRUT>
					  <v13:rut>'.$rut.'</v13:rut>
					  <!--Optional:-->
					  <v13:dv>'.$digito.'</v13:dv>
				   </v1:insuredRUT>
				   <v1:partnerRUT>
					  <v13:rut>078809780</v13:rut>
					  <!--Optional:-->
					  <v13:dv>8</v13:dv>
				   </v1:partnerRUT>
				</v1:GetIndividualizedPolicyDocument>
			 </v1:Body>
		  </v1:get_IndividualizedPolicyDocument_REQ>
	   </soap:Body>
	</soap:Envelope>';

	$soap_do = curl_init();
	curl_setopt($soap_do, CURLOPT_URL,            "https://216.72.183.110:8443/EBS/IndividualizedPolicyDocument");
	//curl_setopt($soap_do, CURLOPT_URL,            "https://10.96.17.45:8443/EBS/IndividualizedPolicyDocument");
	curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 60);
	curl_setopt($soap_do, CURLOPT_TIMEOUT,        60);
	curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
	curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($soap_do, CURLOPT_POST,           true );            
	curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $envelope); 
	curl_setopt($soap_do, CURLOPT_VERBOSE, TRUE); 
	curl_setopt($soap_do, CURLOPT_HTTPHEADER, array("Content-Type: application/soap+xml","SOAPAction: http://cardif.cl/EBS/IndividualizedPolicyDocument/get", "Content-length: ".strlen($envelope))); 

	$result = curl_exec($soap_do);
	curl_close($soap_do);
	$dom = new DOMDocument();

	$dom->loadXML($result);
	$data = $dom->getElementsByTagNameNS('http://cardif.cl/Schema/EBO/OperationStatus/v1.0','OperationStatus')->item(0);
		if(!$data){
			return "NN";
		}else{
			$datadoc = $dom->getElementsByTagNameNS('http://cardif.cl/Schema/EBM/IndividualizedPolicyDocument/get/v1.0','get_IndividualizedPolicyDocument_RSP')->item(0);
			$doc64 = $datadoc->getElementsByTagNameNS('http://cardif.cl/Schema/EBM/IndividualizedPolicyDocument/get/v1.0', 'generatedPDFBase64')->item(0)->nodeValue;
			$docDecoded = base64_decode($doc64);
			$file = 'pdf/'.$rut.'-'.$digito.'_'.$propuesta.'.pdf';
			file_put_contents($file, $docDecoded);
			$nombre = $rut.'-'.$digito.'_'.$propuesta.'.pdf';
			$mensaje= '<table width="540" align="center">
                <tbody><tr>
                   <td>
                    </td><td>
            <table cellpadding="0" cellspacing="0" border="0" width="540" style="background:#f3791f">
                <tbody>
                    <tr>
                        <td height="15px">
                        </td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="540" align="center" style="border:solid 1px #e2e6ea;background:#f7f4ef;padding:20px;margin:0 auto">
                <tbody>
                    <tr>
                        <td>  
                            <table>
                                <tbody>
                                    <tr>
                                        <td style="font-family:Arial,Helvetica,sans-serif;font-size:14px;color:#595959;padding:0 30px 20px;vertical-align:bottom" width="360">
                                            '.$cliente_nombre.',
                                        </td>
                                        <td style="padding-bottom:20px">
                                            <img src="https://banco.itau.cl/publicThemeStatic/themes/publicTheme/css/publico/images/logo-itau.png" alt="" width="45" class="CToWUd">
                                        </td>
                                    </tr>
                                </tbody>
                            </table> 
                        </td>
                    </tr>
                    <tr style="background: #FFF;">
                        <td align="center" valign="top" height="45"> </td>
                    </tr>
                    <tr style="background: #FFF;">
                        <td style="text-align: center; font-size:22px; color:#373e47; padding: 0 30px; font-family: Arial, Helvetica, sans-serif;">
                            &#161;Tu requerimiento ha finalizado exitosamente&#33;
                        </td>
                    </tr>

                    <tr style="background:#fff">
                        <td align="center" valign="top" height="30"> </td>
                    </tr>
                    <tr style="background:#fff">
                        <td style="font-family:Arial,Helvetica,sans-serif; color:#616161; font-size:14px;padding:0 30px;line-height:20px;">
                           Adjunto encontrar&aacute;s la copia de p&oacute;liza de tu seguro <strong>'.$nom_seguro.'</strong>. 
                        </td>          
                    </tr>
                    <tr style="background:#fff">
                        <td align="center" valign="top" height="20"> </td>
                    </tr>
                    <tr style="background:#fff">
                        <td style="color:#616161;font-size:14px;padding:0 30px;line-height:20px; font-family:Arial,Helvetica,sans-serif;">
                            Para m&aacute;s informaci&oacute;n comun&iacute;cate a: 
                        </td>          
                    </tr>
                    <tr style="background:#fff">
                        <td align="center" valign="top" height="10"> </td>
                    </tr>
                    <tr style="background:#fff">
                        <td>
                            <table>
                                <tbody>
                                <tr>
                                    <td align="center" style="color:#6b6b6b;font-size:12px;padding-left:30px">
                                    <img border="0" width="15" id="m_6692346763504116436_x0000_i1026" src="https://ci5.googleusercontent.com/proxy/XXXcFuFgj-knyUH3KbSZcKhU0nn3orwHXE5xMkq1MOjg_9XhAOGoz0Y5Mc4eljVkjs-3-c1cY8-DDz_Pr0h1Syjdi0tO5ikB=s0-d-e1-ft#http://itaucomercialqa.alwayson.cl/img/celular.png" class="CToWUd">
                                    </td>
                                    <td style="color:#6b6b6b; font-size:14px;padding:0 15px;line-height:20px; font-family:Arial,Helvetica,sans-serif;">
                                        <a href="tel:+56%202%202686%200999" value="+56226860999" target="_blank" style="text-decoration: none; color: #6b6b6b;"><strong>600 600 1200</strong></a> 
                                    </td>

                                </tr>
                            </tbody></table>
                        </td>          
                    </tr>
                    <tr style="background:#fff">
                        <td align="center" valign="top" height="20"> </td>
                    </tr>
                    <tr style="background:#fff">
                        <td style="color:#6b6b6b;font-size:10px;padding:0 30px;font-weight:bold; font-family:Arial,Helvetica,sans-serif;">
                            Este email fue generado autom&aacute;ticamente, por favor no respondas este mensaje. Ante cualquier duda, cont&aacute;ctate con tu ejecutivo de cuentas o Ita&uacute; Corredora de Seguros.
                        </td>          
                    </tr>
                    <tr style="background:#fff">
                        <td align="center" valign="top" height="20"> </td>
                    </tr>
                    <tr>
                        <td height="30px"></td>
                    </tr>

                    <tr>
                        <td>
                            <table style="border-collapse:collapse" width="498">

                                <tbody><tr style="background:#fff">
                                    <td align="center" style="padding-left:30px">
                                        <img src="https://ci6.googleusercontent.com/proxy/aP_L8N-_elPitNOmoDUAAm8La8qhTTOg9dANn0JVWQJyootM3c_0qKGCsnEg4ST4F3bKM2D9n3HeaRI6y1Udy33HNVgoS4_nkN3f2C8wCdrK=s0-d-e1-ft#http://desarrollo.alwayson.es/ITAU-Consulta/lock-e-mail.png" alt="" width="25" style="text-align:center" class="CToWUd">
                                        <p style="font-size:10px;margin-top:3px;color:#f3791f; font-family:Arial,Helvetica,sans-serif;">Email seguro</p>
                                    </td>
                                    <td style="font-family:Arial,Helvetica,sans-serif;font-size:10px;line-height:15px;padding:10px 0 5px 0;color:#6b6b6b;background:#fff">
                                            <ul style="padding-left:25px">
                                                <li style="color:#f3791f"><span style="color:#595959">Siempre escribe <a href="http://www.itau.cl" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=es&amp;q=http://www.itau.cl&amp;source=gmail&amp;ust=1513080086695000&amp;usg=AFQjCNGSO6XTcNiOG96XtOKT_Vq5tYRX2Q" style="color:#6b6b6b; text-decoration: none;"><b>www<font style="font-size:1px">&nbsp;</font>.itau.<font style="font-size:1px">&nbsp;</font>cl</b></a> en la barra del navegador.</span></li>
                                                <li style="color:#f3791f"><span style="color:#595959">Nunca te enviaremos correos con links.</span></li>
                                                <li style="color:#f3791f"><span style="color:#595959">Modifica regularmente las contrase&ntilde;as de tus tarjetas y clave de acceso.</span></li>
                                                <li style="color:#f3791f"><span style="color:#595959">Nunca pediremos datos personales, claves o informaci&oacute;n de coordenadas.</span></li>
                                            </ul>
                                    </td>
                                </tr>
                            </tbody></table>

                        </td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="540" style="background:#d0d0d0">
                <tbody><tr>
                    <td height="15px">
                    </td>
                </tr>
            </tbody>
            </table>
            
        </td>
    </tr>
</tbody>
</table>';

	$filename=$nombre;
	$file = "pdf/".$filename;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    $content = chunk_split(base64_encode($content));


    $Msubjet='Tu copia de póliza del seguro '.$nom_seguro.' ha sido enviada con éxito';
    $Morigen='itaucorredoresdeseguros@itau.cl';
    $Mdestino=$mail;
    $MCC='';
    $MCCO='';
    $MnombreAdjunto=$nombre;
    $Mcuerpo='<![CDATA['.$mensaje.']]>';
    $Madjunto='<![CDATA['.$content.']]>';
    $enviarCorreo=enviarMailPorItau($Msubjet,$Morigen,$Mdestino,$MCC,$MCCO,$Mcuerpo,$Madjunto,$MnombreAdjunto);
			return $nombre;
		}
}
?>