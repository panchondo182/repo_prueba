<?php
require_once 'class/config.php';
require_once 'class/generales_class.php';
require_once 'class/generales_validacionesCliente.php';

$dat                = isset($_GET["data"])      ? $_GET["data"] : '';
$cuenta             = isset($_GET["cuenta"])    ? $_GET["cuenta"] : '';
$banco              = isset($_GET["banco"])     ? $_GET["banco"] : '';
$email              = isset($_GET["email"])     ? $_GET["email"] : '';
$telefono           = isset($_GET["tel"])       ? $_GET["tel"] : '';
$mot                = isset($_GET["mot"])       ? $_GET["mot"] : '';
$requerimiento      = isset($_GET["req"])       ? $_GET["req"] : '';
$poliza             = isset($_GET["pol"])       ? $_GET["pol"] : '';
$codigo_producto    = '';
$idcontrato         = '';
$data               = decrypt($dat);

$mysqli = new mysqli($DB['host'], $DB['user'], $DB['pass'], $DB['useBI']);

//echo $poliza;
    //datos para insert
    $datos_poliza = explode("*-*", $poliza);
    $codigo_producto = $datos_poliza[2];
    $npoliza = $datos_poliza[0];
    $asociado_credito = $datos_poliza[1];
    $num_credito = $datos_poliza[3];
    $num_poliza = $datos_poliza[4];
    $flujo = $datos_poliza[5];
    $idcontrato = $datos_poliza[6];

$salida[0]='';
$salida[1]='inicio';

if (mysqli_connect_errno()) {
    $salida[0]='';
    $salida[1]='error conexion';
}else{
    //consultamos si existe el mismo req para el mismo cliente y la misma poliza dependiendo el flujo
    if($flujo=='V'){
        //comparo numero de poliza
        $query2="SELECT * FROM requerimientos WHERE rut='".$_GET['data']."' AND poliza='".$num_poliza."' AND bloqueo=1 AND requerimiento='".$requerimiento."' AND requerimiento in ('eliminacion_de_seguro','devolucion_de_prima','copia_de_poliza_o_certificado')";
    }
    if($flujo=='W'){
        //comparo idcontrato
        $query2="SELECT * FROM requerimientos WHERE rut='".$_GET['data']."' AND idcontrato='".$idcontrato."' AND bloqueo=1 AND requerimiento='".$requerimiento."' AND requerimiento in ('eliminacion_de_seguro','devolucion_de_prima','copia_de_poliza_o_certificado')";

    }
    $result2=mysqli_query($mysqli, $query2);
    $cpoliza2 = mysqli_fetch_array($result2);
    $nfilas = mysqli_num_rows($result2);   

    //consultar a cliente
    $query="SELECT * FROM clientesNew WHERE rut='".$dat."'";      
    $result=mysqli_query($mysqli, $query);
    $cpoliza = mysqli_fetch_array($result);
    //armar nombres
    $nombreCliente = $cpoliza['nombre'];
    $apellidoCliente = $cpoliza['apellidop']." ".$cpoliza['apellidom'];
    $cliente_nombre = $nombreCliente.' '.$apellidoCliente;

//si existe a la salida se le asigna la poliza
    if($nfilas>0){
            $salida[0] = $cpoliza2['id'].'*-*'.$cpoliza2['nombreseg'];
            $salida[1] = $cliente_nombre;
            //exit();
    }else{
        //if($salida[0]!=$cpoliza2['poliza']){
            //$query="SELECT * FROM seguros WHERE rut='$dat'";   
            //$rutCliente = $cpoliza['rut'];
            //$codproductoCliente = $cpoliza['cod_producto'];

            
            //$req = buscar_requerimiento($requerimiento);
            //$mot = $motivo;
            if($_GET['req']=='copia_de_poliza_o_certificado'){
                $nuevafecha = strtotime('+5 day', strtotime($fechahoy));
            }
            if($_GET['req']=='eliminacion_de_seguro'){
                $nuevafecha = strtotime('+10 day', strtotime($fechahoy));
            }
            if($_GET['req']=='devolucion_de_prima'){
                $nuevafecha = strtotime('+11 day', strtotime($fechahoy));
            }
            $nuevafecha = date('Y-m-j',$nuevafecha);

            if($codigo_producto!=''){
                    $query="insert into bancoitau.requerimientos (rut,nombrecli,codigoseg,nombreseg,tipocre,numcre,poliza,requerimiento,motivo,fechaingreso,email,telefono,banco,ctacte,estado,flujo,idcontrato,sla_new)";
                    $query.="values('$dat','$cliente_nombre','$codigo_producto','$npoliza','$asociado_credito','$num_credito','$num_poliza','$requerimiento','$mot',curdate(),'$email','$telefono','$banco','$cuenta','Pendiente sin  Firma','$flujo','$idcontrato','$nuevafecha')";
                    $result = $mysqli->query($query);
                    $ultimoId = $mysqli->insert_id;
                    // $salida[0]=$npoliza;
                    $salida[0]=$ultimoId.'*-*'.$npoliza;
                    $salida[1]=$cliente_nombre;
                    //$salida[2]=$ultimoId;
            }else{
                $salida[0]='';
                $salida[1]=$cliente_nombre;
                //$salida[2]='';
            }
        
        
        }  

    //}
    $mysqli->close();

}
print json_encode($salida, JSON_UNESCAPED_UNICODE);
?>