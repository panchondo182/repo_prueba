<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv= "X-UA-Compatible" content="IE=9"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Perfil Asegurado</title>
  <link rel="stylesheet" href="assets/css/estilos.css">
</head>
<body>

<div class="container-main">

<section class="tabs">
  <input id="tab-1" type="radio" name="radio-set" class="tab-selector-1" checked="checked" />
  <label for="tab-1" class="tab-label-1">Información del Cliente</label>
  
  <input id="tab-2" type="radio" name="radio-set" class="tab-selector-2" />
  <label for="tab-2" class="tab-label-2">Seguros Contratados</label>
  
  <input id="tab-3" type="radio" name="radio-set" class="tab-selector-3" />
  <label for="tab-3" class="tab-label-3">Servicio Post-Venta</label>
  <div class="content-tabs">
    <div class="content-info-cliente">
      <section class="section-info-cliente">
        <ul class="info-cliente-col-1">
          <li class="info-cliente-name">Pablo Gómez</li>
          <li><strong>Rut:</strong> 10.905.434-9</li>
          <li>Personal BANK</li>
        </ul>

        <ul class="info-cliente-col-2">
          <li><strong>Edad:</strong> 42 años</li>
          <li><strong>Profesión:</strong> Ing. Civil Industrial</li>
          <li><strong>Trabajador:</strong> Dependiente</li>
          <li><strong>Empresa:</strong> Itaú Chile</li>
          <li><strong>Hijos:</strong> 4 hijos</li>
        </ul>

        <ul class="info-cliente-col-3">
          <li><strong>Viajes:</strong> 3 viajes al año</li>
          <li><strong>E-mail:</strong> nombre@cdf.cl</li>
          <li><strong>Telef. Personal:</strong> +56 9 1234 5678</li>
          <li><strong>Telef. Comercial:</strong> +56 9 1234 5678</li>
          <li><strong>Ejecutivo:</strong> Sandra Soms</li>
        </ul>
      </section>

      <section class="box-seguros-recomendados">
        
        <h4 class="title-seguros-recomendados">Seguros Recomendados</h4>
       
        <ul class="seguros-recomendados-col-1">
          
          <li>
            <ul>
              <li><span>Vida</span></li>
            </ul>

            <ul>
              <li>Vida</li>
            </ul>
          </li>

          <li>
            <ul>
              <span>Protección Financiera</span>
            </ul>

            <ul>
              <li>Protección integral Tarjeta</li>
            </ul>
          </li>

          <li>
            <ul>
              <span>Salud</span>
            </ul>

            <ul>
              <li>Oncológico</li>
              <li>Enfermedades y Cirugías graves</li>
              <li>Cardiológico</li>
              <li>Renta Hospitalaria por Accidente</li>
              <li>Emergencia Accidental</li>
              <li>Bike</li>
              <li>Mascotas</li>
            </ul>
          </li>
          
        </ul>

        <ul class="seguros-recomendados-col-2">
          <li>
            <ul>
              <li><span>Hogar</span></li>
            </ul>

            <ul>
              <li>Hogar Contenido</li>
            </ul>
          </li>
          
          <li>
            <ul>
              <li><span>Asistencias</span></li>
            </ul>

            <ul>
              <li>Viaje Protegido</li>
              <li>Asistencia Premium</li>
            </ul>
          </li>
          
          <li>
            <ul>
              <li><span>Automotriz</span></li>
            </ul>

            <ul>
              <li>SOAP 2017 Compra</li>
              <li>SOAP 2017 Canje Itaú Dólares</li>
              <li>SOAP 2017 Canje Puntos</li>
            </ul>
          </li>
        </ul>
      </section>

      <section class="boton-volver-center">
        <button class="boton-style boton-volver" type="button">Volver</button>
      </section>


    </div>

    <div class="content-seguros-contrata">
      <h2>Tab 2</h2>
      <p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
    </div>

    <div class="content-servicios">
      <h2>Tab 3</h2>
      <p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
      <p>Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.</p>
    </div>
    
  </div>
</section>

</div>
  
</body>
</html>