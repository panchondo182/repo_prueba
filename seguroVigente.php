<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv= "X-UA-Compatible" content="IE=9"/>
    <title>Portal de Seguros</title>
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="assets/js/jquery-function.js"></script>   
</head>
<body>
    <header>
    <div class="logo_itau">
       <img src="assets/img/logo-itau.png" alt="">    
    </div>
    <div class="header_right">
        Plataforma Comercial
    </div>
    </header>
   <div class="container">
        <div class="menu">
            <div class="top_menu"><h4>Portal de Seguros</h4></div>
            <div class="menu_box">
                <ul>
                    <li><a href=""><strong class="menu_active">Perfil Asegurado</strong></a></li>
                    <li><a href="">Nuestros Productos</a></li>
                </ul>
            </div>
        </div>
        <div class="modulo">       
            <div class="titulo">
            <h4 class="blanco">Estado: Seguro Vigente</h4>
            </div>
            <div class="contenido">
                <div class="top_seguros">
                    <div class="top_left">
                       <img src="assets/img/seguro_vida.png" alt="">
                       <h1 class="top_titulo">Seguro Vida</h1>
                       <h2 class="subtitulo">Vigente</h2>
                    </div>
                    <div class="top_right">
                       <h4>Fecha de Contratación</h4>
                       <p>20/04/2016</p>
                    </div>          
                </div>
                   <div class="resumen">
                       <div class="marginr8">
                             <div class="det_uf">
                             <strong>$8.736 * (UF 0,33)</strong><br>
                              Mensuales
                             </div>
                             <div class="det_cob">
                             <h4>Detalles Cobertura</h4>
                                 <ul class="bullet">
                                     <li> 
                                     <strong>Fallecimiento Accidental</strong><br>UF 1000
                                     </li>
                                 </ul>
                             </div>
                       </div>
                       <div class="marginr8">
                           <h4>Datos Contratante</h4>
                           <p><strong>Nombre: </strong>María José Vallejos Catalán</p>
                           <p><strong>RUT: </strong>10.905.434-9</p>
                           <h4 class="margint3">Datos de Pago</h4>
                           <p><strong>Tarjeta N°:</strong> 0000 0000 0000 0000</p>
                           <p><strong>Tipo de Tarjeta:</strong> Visa</p>
                           <p><strong>Fecha de Vencimiento:</strong> 20/10/2020</p>
                       </div>
                       <div class="">
                           <h4>Datos Contacto</h4>
                           <p><strong>Dirección: </strong>Av. Sucre 2725, Ñuñoa, Santiago.</p>
                           <p><strong>Teléfono: </strong>+56 2 0000 0000 - +56 9 0000 0000</p>
                           <p><strong>E-mail: </strong>nombre@dominio.com</p>
                       </div>
                   </div>
                   <!-- <a href="assets/docs/MIX-ALEMAN-1114.pdf" class="detalle underline"><i class="i_terms"></i>Detalle de Póliza</a> -->
                   <div class="botones">
                        <a class="btn_gris marginr3 displayib" href="javascript:history.back(-1);">Volver</a>           
                        <a class="btn_naranja displayib" href="01informacion_cliente.html" target="_self">Ir al Cliente</a>
                   </div>
              </div>
          </div>           
    </div>     
</body>
</html>