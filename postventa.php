<?php
session_start();
$cargo = isset($_SESSION['cargo']) ? $_SESSION['cargo'] : ''; 
$data = isset($_GET["data"]) ? $_GET["data"] : ''; 
switch($cargo){
    case 'EJECUTIVO':
        //header("location: segurosContratados.php?data=".$data);
        break;
    case 'AD_CTACTE':
        //header("location: segurosContratados.php?data=".$data);
        break;
	case 'JE_OPERA':
        //header("location: segurosContratados.php?data=".$data);
        break;
	case 'TESORERO':
        //header("location: segurosContratados.php?data=".$data);
        break;
    case 'AGENTE':
        //header("location: segurosContratados.php?data=".$data);
        break;
    case 'BTEL':
        header("location: bpostventa.php?data=".$data);
        break;
    case '':
        session_destroy();
        header("location: mensajeError.php?codigo=3535");
        break;
    }

require_once 'class/config.php';
require_once 'class/generales_class.php';
require_once 'class/generales_validacionesCliente.php';
include ('curlWrap.php');
//$data = $_GET["data"];

//$postVenta = listaPostVenta($data,$DB);
 $mysqli = new mysqli($DB['host'], $DB['user'], $DB['pass'], $DB['useBI']);
 $query="SELECT * FROM requerimientos WHERE rut='$data' and estado in ('Solicitado','Pendiente de Firma') order by id desc";      
 $result=mysqli_query($mysqli, $query);

 $row_cnt = mysqli_num_rows($result);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Portal de Seguros</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9"/>
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/css/postventa.css">

<style>
.even { background-color:#F5F5F5; }
.odd { background-color:#fff; }
</style>

</head>
<body>
    <header>
        <div class="logo_itau">
            <img src="assets/img/logo-itau.png" alt="">

        </div>
        <div class="header_right">
            Plataforma Comercial
        </div>
    </header>
    <div class="container">
        <div class="container_menu">
            <div class="menu">
                 <div class="top_menu"><h4>Portal de Seguros</h4></div>
                 <div class="menu_box">
                     <ul>
                        <li class="margen-bottom-10"><a href="informacion.php?data=<?php echo $data; ?>">Información del cliente</a></li>
                        <li class="margen-bottom-10"><a href="segurosContratados.php?data=<?php echo $data;?>">Seguros contratados</a></li>
                        <li class="margen-bottom-10"><a href="postventa.php?data=<?php echo $data; ?>"><strong class="menu_active">Post-venta</a></strong></li>
                     </ul>
                </div>
            </div>
        </div>
        <?php //include('menuLateral.php');?>
        <div class="modulo">
            <nav>
                <ul class="tabs">
                    <li class=""><a href="informacion.php?data=<?php echo $data; ?>">Información del cliente</a></li>
                    <li><a href="segurosContratados.php?data=<?php echo $data;?>">Seguros contratados</a></li>
                    <li class="tabs_active"><a href="#" name="tab3">Post-venta</a></li> 
                </ul>
            </nav>
            <?php if($row_cnt>0){?>
            <div class="contenido">
                <h3>Requerimientos</h3>
                <div class="postVenta">
                    <div>
                        <a href="requerimiento.php?data=<?php echo $data; ?>" class="btn_naranja displayb floatr">Nuevo requerimiento</a>
                    </div>
                    <div class="clearb"></div>
                    <div>
                </div>                   

                    <?php 
                    while ($poliza = mysqli_fetch_array($result))
                    {  
                        $num_ticket =$poliza['idzendesk'];
                        if($num_ticket!=''){
                            $return = curlWrap("/tickets/".$num_ticket.".json", '', "GET");//////////////////////////////////////////////////
                            //print_r($return);
                            $estado_detalle = $return->ticket->status;
                            $estado_fecha = $return->ticket->updated_at;
                        }

                        switch($poliza['requerimiento']){
                            case 'copia_de_poliza_o_certificado':
                                $reque='Copia de p&oacute;liza';
                                break;
                            case 'eliminacion_de_seguro':
                                $reque='Eliminar seguro';
                                break;
                            case 'devolucion_de_prima':
                                $reque='Devoluci&oacute;n de prima';
                                break;
                        }
                    $contEstilo=0;

                if($poliza['flujo']=='W'){
                    /* +++++++++++  ++++ */
                    $mysqli2 = new mysqli($DB['host'], $DB['user'], $DB['pass'], $DB['usePP']);
                $query3="select * from ".$poliza['codigoseg']." where id='$poliza[idcontrato]' order by id desc";
                    //echo $query3;
                    if ($result3 = $mysqli2->query($query3)) {
                        while ($prod = $result3->fetch_assoc()) {   

                            $c_nomcli = $prod['nombre'];
                            $c_rut = formateo_rut(decrypt($prod['rut']));
                            $c_fecini = $prod['grabacion'];

                            $num_tarjetaExp = explode('*-*', decrypt($prod["num_tarjeta"]));
                            $num_tarjeta = "****-****-****-".$num_tarjetaExp[3];
                            $tarjeta = $prod['tarjeta'];
                            $banco = $prod['banco'];
                            $detVencimiento = explode('*-*', $prod["vencimiento"]);  
                            $vencimiento = $detVencimiento[0]."/".$detVencimiento[1];  

                            if($poliza['codigoseg']=='SVBBSS023'){
                                $c_poliza = '21994';
                                $c_nomcomp = 'Consorcio';
                                if($prod['plan']=='plan1'){
                                    $c_prima = '0.329';
                                }else{
                                    $c_prima = '0.506';
                                }
                            }
                            if($poliza['codigoseg']=='SVBBSS019'){
                                    $c_prima = '0.15';
                                    $c_poliza = '5515922';
                                    $c_nomcomp = 'Sura';                              
                            }
                            if($poliza['codigoseg']=='SVBBSS017'){
                                $c_nomcomp = 'Ita&uacute; Vida';
                                if($prod['plan']=='plan1'){
                                    $c_prima = '0.33';
                                }else{
                                    $c_prima = '0.99';
                                }
                            }
                            //auto
                            if($poliza['codigoseg']=='SVBBSS035'){
                                $c_prima = $prod['primaunica'];
                                $c_nomcomp = $prod['aseguradora'];;
                            }
                            //viaje buscar
                            if($poliza['codigoseg']=='SVBBSS026'){
                                $c_prima = $prod['primaunica'];
                                $c_nomcomp = 'Ita&uacute; Vida';
                            }
                            
                        }
                    }

                    switch($poliza['codigoseg']){
                        case "SVBBSS019":
                            $c_nombre='Protecci&oacute;n Tradicional';
                            $icono = "i_proteccion";
                            $segprima = '5515922';
                            break;
                        case "SVBBSS017":
                            $c_nombre='Vida con Bonificaci&oacute;n';
                            $icono = "i_vida";
                            break;
                        case "SVBBSS023":
                            $c_nombre='Hogar Contenido';
                            $icono = "i_hogar";
                            $segprima = '2199';
                            break;
                        case "SVBBSS026":
                            $c_nombre='Viaje Pretegido Plus';
                            $icono = "i_asistencias";
                            break;
                        case "SVBBSS030":
                            $c_nombre='Protecci&oacute;n Preferente';
                            $icono = "i_proteccion";
                            break;
                        case "SVBBSS035":
                            $c_nombre='Seguro Automotriz';
                            $icono = "i_auto";
                            break;
                    }
                }
                if($poliza['flujo']=='V'){
                $query2="select * from codigos WHERE codigo='$poliza[codigoseg]' LIMIT 1";
				//echo $query2;
                if ($result2 = $mysqli->query($query2)) {
                while ($codigo = $result2->fetch_assoc()) {
                    $c_nombre=$codigo['nombre'];
                    $c_plan=$codigo['plan'];
                    $c_tipo=$codigo['categoria'];
                    switch($c_tipo){           
                        case "Seguros de Proteccion Financiera":
                            $icono = "i_proteccion";
                            break;
                        case "Seguros de Vida":
                            $icono = "i_vida";
                            break;
                        case "Seguros de Salud":
                            $icono = "i_salud";
                            break;
                        case "Seguros de Hogar":
                            $icono = "i_hogar";
                            break;
                        case "Seguros de Asistencias":
                            $icono = "i_asistencias";
                            break;
                        case "Seguro Automotriz":
                            $icono = "i_auto";
                            break;
                        default:
                            $icono = "";
                            break;
                    }
                }
                $result2->free();
            }
        }
            $query2="select * from seguros WHERE cod_producto='$poliza[codigoseg]' and rut='$poliza[rut]' and poliza='$poliza[poliza]' LIMIT 1";
            //echo $query2;
                if ($result2 = $mysqli->query($query2)) {
                while ($codigo = $result2->fetch_assoc()) {

                    $c_prima=$codigo['bruta'];
                    //echo $codigo['nom_producto']."dfsdfsd";
					if($c_nombre=='No tiene' or $c_nombre=='') $c_nombre = $codigo['nom_producto'];
                    //echo $c_nombre."aaaaaaaaaaa";

                }
                $result2->free();
            }
                        ?>
                     <table class="font14 margint2">
                        <tr class="<?=($c++%2==1) ? 'odd' : 'even' ?>">
                            <td><i class="<?php echo $icono; ?>"></i></td>
                            <td>
                                <ul>
                                    <li><strong><?php echo $c_nombre; ?></strong></li>
                                    <li><strong>Prima bruta mensual:</strong>
                                    <?php 
                                    
                                    if($c_prima>150){
                                        $signo = '$';
                                        $vvalor = number_format($c_prima, 0, '', '.');
                                    }else{
                                        $signo = 'UF';
                                        $valor = number_format($c_prima,3, ',',' ');
                                        $vvalor = str_replace(".",",",$valor);
                                    }
                                    if($vvalor =='') echo "No Informado"; else echo $signo.' '.$vvalor;?>
   
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <ul class="nopadding">
                                    <li><strong>N° de ticket:  <?php if($poliza["idzendesk"]==''){echo 'Pendiente de firma';}else{echo $poliza["idzendesk"];} ?></strong></li>
                                    <li><strong>Requerimiento:</strong> <?php echo $reque; ?> </li>
                                </ul>
                            </td>
                            <td class="text_center">
                                <?php 
                                //$num_ticket.' - '.$estado_detalle.' - '.$estado_fecha;
                                switch($estado_detalle){
                                    case 'new':
                                        $estado_zend = 'Nuevo';
                                        break;
                                    case 'open':
                                        $estado_zend = 'Abierto';
                                        break;
                                    case 'pending':
                                        $estado_zend = 'Pendiente';
                                        break;
                                    case 'solved':
                                        $estado_zend = 'Resuelto';
                                        break;
                                    case 'closed':
                                        $estado_zend = 'Cerrado';
                                        break;
                                    case 'hold':
                                        $estado_zend = 'Sostenedor';
                                        break;    
                                    } 
                                ?>

                                <ul class="">
                                    <li><strong><?=$estado_zend;?></strong></li>
                                    <li>
                                    <?php 
                                            echo $estado_fecha;
                                            /*$date=date_create($estado_fecha);
                                            echo date_format($date,"d-m-Y");
                                            */
                                     ?></li>
                                </ul>
                            </td>
                            <td class="fontbold">
                            
                            <a href="postventaDetalle.php?data=<?php echo $data; ?>&idreq=<?php echo $poliza["id"]; ?>" class="naranja">Ver detalle</a></td>
                        </tr>
                    </table>
                    <?php 


                    }//while ?>
                </div>
            </div>
            <?php }else{?>
            <div class="contenido">
                <div class="postVenta">
                    <div>
                        <a href="requerimiento.php?data=<?php echo $data; ?>" class="btn_naranja displayb floatr">Nuevo requerimiento</a>
                    </div>
                    <div class="clearb"></div>

                    <div>  

                    <div class="message margint5">
                        <img src="assets/img/sin-requerimiento.png" width="80" alt="" class="margins0">
                        <p class="text_center margintb font16"><strong>El cliente no tiene requerimientos</strong></p>

                        <a class="btn_naranja displayb margintb margins0" href="informacion.php?data=<?php echo $data; ?>">Ir al cliente</a>
                    </div>
                </div> 

                </div>
            </div>
            <?php }?>

        </div>
    </div>
</body>
</html>