<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv= "X-UA-Compatible" content="IE=9"/>
    <title>Portal de Seguros</title>
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
</head>

<body>
    <header>
    <div class="logo_itau">
       <img src="assets/img/logo-itau.png" alt="">     
    </div>
        <div class="header_right">
            Plataforma Comercial
        </div>
    </header>
    <div class="container">
        <div class="container_menu">
            <div class="menu">
                 <div class="top_menu"><h4>Portal de Seguros</h4></div>
                 <div class="menu_box">
                     <ul>
                        <li><a href=""><strong class="menu_active">Perfil Asegurado</strong></a></li>
                        <li><a href="">Nuestros Productos</a></li>
                     </ul>
                </div>
            </div>
        </div>
        <div class="modulo">
            <nav>
                <ul class="tabs">
                    <li class=""><a href="01informacion_cliente.html">Información del cliente</a></li>
                    <li class="tabs_active"><a href="06seguros_contratados.html">Seguros contratados</a></li>
                    <!--<li><a href="#" name="tab3">Servicio Post-Venta</a></li> -->
                </ul>
            </nav>
            <div class="contenido">
                <div>
                    <div class="message margint5">
                        <img src="assets/img/ico-seguros.png" alt="" class="margins0">
                        <p class="text_center margintb font16"><strong>El cliente no posee seguros contratados</strong></p>

                        <a class="btn_naranja displayb margintb margins0" href="01informacion_cliente.html">Ir al Cliente</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>