<?php

//DATOS ITAU
	$entrada['itau']['estado']         = isset($_POST["estado"])        ? $_POST["estado"] : '';
    $entrada['itau']['descripcion']    = isset($_POST["descripcion"])   ? $_POST["descripcion"] : '';
    $entrada['itau']['token']          = isset($_POST["token"])         ? $_POST["token"] : '';
    $entrada['itau']['incremental']    = isset($_POST["incremental"])   ? $_POST["incremental"] : '';
    $entrada['itau']['userid']         = isset($_POST["userid"])        ? $_POST["userid"] : '';
    $entrada['itau']['apenom']         = isset($_POST["apenom"])        ? $_POST["apenom"] : '';
    $entrada['itau']['cargo']          = isset($_POST["cargo"])         ? $_POST["cargo"] : '';
    $entrada['itau']['suc']            = isset($_POST["suc"])           ? $_POST["suc"] : '';
    $entrada['itau']['nomsuc']         = isset($_POST["nomsuc"])        ? $_POST["nomsuc"] : '';
    $entrada['itau']['vendedor']       = isset($_POST["vendedor"])      ? $_POST["vendedor"] : '';
    $entrada['itau']['pc'] 			   = isset($_POST["pc"])            ? $_POST["pc"] : '';

//DATOS ALWAYS

    $entrada['alwayson']['idSeguro']   = isset($_POST["idSeguro"])      ? $_POST["idSeguro"] : '';
    $entrada['alwayson']['nomSeguro']  = isset($_POST["nomSeguro"])     ? $_POST["nomSeguro"] : '';
    $entrada['alwayson']['idVenta']    = isset($_POST["idVenta"])       ? $_POST["idVenta"] : '';
    $entrada['alwayson']['rut'] 	   = isset($_POST["rut"])           ? $_POST["rut"] : '';
    $entrada['alwayson']['rutenc'] 	   = isset($_POST["rutenc"])        ? $_POST["rutenc"] : '';

//HTML

    $entrada['html']                   = isset($_POST["html"])      ? base64_decode($_POST["html"]) : '';

//IMPRIMIR VARIABLES

var_dump($entrada);

//echo $entrada['html'];

