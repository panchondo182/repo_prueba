<?php
  require_once 'class/generales_class.php';
  require_once 'class/generales_validacionesCliente.php';
    
  if(!empty($_POST)){
    $mensaje= "ENTRO";
    $rut = isset($_POST["rut-cliente"]) ? $_POST["rut-cliente"] : '';
    $valRut = isset($_POST["valRut"]) ? $_POST["valRut"] : '';
        
    if($rut!='' && $valRut=='ok'){
      $datos=comun_verificaCliente(encrypt($rut),$rut);
      //var_dump($datos);
        
      if($datos['respuesta'] == 'rut ok'){
        $datosEncriptados= serialize($datos);
        //$datosEncriptados=str_replace('"', '|',$datosEncriptados); 
        echo "<script>location.href='informacion.php?datosEncriptados=".encrypt($datosEncriptados)."'</script>";
        }
    }
  }

  ?>
    
  <div class="modulo_home margen-top-72">
    <div class="margint120">
      <div class="top_busqueda">
          <h4 class="blanco">Búsqueda Clientes</h4>
      </div>
    <div class="box_busqueda">
      <form action="home.php" id="busquedaClientes" name="busquedaClientes" method="post">
        <label class="marginr4"><strong class="azul">RUT</strong></label>
        <input type="text" class="input_login" id="rut-cliente" onblur="errorCampoVacio(this)" name="rut-cliente" placeholder="RUT Asegurado" autocomplete="off" >
                  
        <div id="errRut" class="inactive" style="margin-top: 5px;"></div>
          <input type="hidden" name="valRut" id="valRut" value="">
          <button class="btn_naranja btn_sm floatr margint2" type="button" onclick="envia()">Buscar</button>

      </form>
     <div class="btm_busqueda">
         <p class="">
            <i class="">Conoce los Seguros Itaú</i>
          </p>
          <a href="" class="btn_naranja displayb">Ir a Productos</a>
     </div>
    </div>
    </div>
  </div>
  
  <script type="text/javascript">
    var input = document.getElementById("rut-cliente");
    var mensajeError = document.getElementById('errRut');

      function errorCampoVacio(rut){
      if (rut.value==''){
       mensajeError.className = 'active';
        document.getElementById('errRut').innerHTML="<h4>*Debe ingresar un RUT</h4>";
        document.getElementById('valRut').value='';
        document.getElementById('rut-cliente').className = "errorRutInput";
      }
    }
    
    
    input.addEventListener('change', function( event ){
      var valor = this.value;
      var rut = new validaRut( valor );

      var mensajeError = document.getElementById('errRut');
      mensajeError.className = 'inactive';

      if( !rut.isValid ){
        // alert('El rut no es valido');
        mensajeError.className = 'active';
        document.getElementById('errRut').innerHTML="<h4>*RUT ingresado inválido</h4>";
          document.getElementById('valRut').value='error';
        document.getElementById('rut-cliente').className = "errorRutInput";
      }
      else {
        // alert('el rut es valido');
        document.getElementById('rut-cliente').className = "ok";
          document.getElementById('valRut').value='ok';
        mensajeError.className = 'inactive';
        console.log(rut.cleanRut);
      }
      
      this.value = rut.cleanRut;
    });
    function envia()
    {
      document.getElementById("busquedaClientes").submit();  
    }
   
  </script>