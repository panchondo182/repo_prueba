<?php
require_once 'class/config.php';
require_once 'class/generales_class.php';
require_once 'class/generales_validacionesCliente.php';
//require_once 'datosCase.php';
session_start();
	$_SESSION['rut']          = isset($_POST["AO_rut"])         ? $_POST["AO_rut"] : $_SESSION['rut'];
	$_SESSION['user']         = isset($_POST["AO_user"])        ? $_POST["AO_user"] : $_SESSION['user'];
    $_SESSION['serverName']   = isset($_POST["AO_serverName"])  ? $_POST["AO_serverName"] : $_SESSION['serverName'];
    $_SESSION['token']        = isset($_POST["AO_Token"])       ? $_POST["AO_Token"] : $_SESSION['token'];
    $_SESSION['incremental']  = isset($_POST["AO_Incremental"]) ? $_POST["AO_Incremental"] : $_SESSION['incremental'];
    $_SESSION['userid']       = isset($_POST["AO_UserID"])      ? $_POST["AO_UserID"] : $_SESSION['userid'];
    $_SESSION['apenom']       = isset($_POST["AO_Apenom"])      ? $_POST["AO_Apenom"] : $_SESSION['apenom'];
    $_SESSION['cargo']        = isset($_POST["AO_Cargo"])       ? $_POST["AO_Cargo"] : $_SESSION['cargo'];
    $_SESSION['suc']          = isset($_POST["AO_Suc"])         ? $_POST["AO_Suc"] : $_SESSION['suc'];
    $_SESSION['nomsuc']       = isset($_POST["AO_NomSuc"])      ? $_POST["AO_NomSuc"] : $_SESSION['nomsuc'];
	$_SESSION['UrlVolver'] 	  = 'https://itaucomercialqa.alwayson.cl/';

  $rutVendedor = $_SESSION['rut_ejecutivo'];
    
  $Rdv = substr($rutVendedor, -1);
  if($Rdv=='k'){
      $Rdv=strtoupper($Rdv);
  }
  $rutVendedor = (substr($rutVendedor, 0, -1)).$Rdv;


//echo $requerimiento;
$poliza = $_GET['pol'];                            
$requerimiento = $_GET['req'];
$motivo = $_GET['mot'];
$email = $_GET['email'];
$telefono = $_GET['tel'];
$banco = $_GET['banco'];
$cuenta = $_GET['cuenta'];
$data = decrypt($_GET['data']);
$dat = $_GET['data'];

$rut=decrypt($dat); 

//echo 'esta es la url de la session: '.$UrlValidaPinItau;

    //MUESTRO DATOS CLIENTE
    $mysqli = new mysqli($DB['host'], $DB['user'], $DB['pass'], $DB['useBI']);

    $query="SELECT * FROM seguros WHERE rut='$dat'";      
    $result=mysqli_query($mysqli, $query);
    $cpoliza = mysqli_fetch_array($result);
     
 
$nombreCliente = $cpoliza['nombre'];
    $apellidoCliente = $cpoliza['apellidos'];
    $cliente_nombre = $nombreCliente.' '.$apellidoCliente;
    $rutCliente = $cpoliza['rut'];
    $codproductoCliente = $cpoliza['cod_producto'];


    //datos para insert
    $porciones = explode("*-*", $poliza);
    $codigo_producto = $porciones[2];
    $npoliza = $porciones[0];
    $asociado_credito = $porciones[1];
    $num_credito = $porciones[3];
    $num_poliza = $porciones[4];
	$flujo = $porciones[5];
    $req = buscar_requerimiento($requerimiento);
    $mot = $motivo;  
//INSERTA REQUERIMIENTO INI
$UrlValidaPinItau=$_SESSION['UrlValidaPinItau'];

$query="insert into bancoitau.requerimientos (rut,nombrecli,codigoseg,nombreseg,tipocre,numcre,poliza,requerimiento,motivo,fechaingreso,email,telefono,banco,ctacte,estado,flujo) ";
$query.="values('$dat','$cliente_nombre','$codigo_producto','$npoliza','$asociado_credito','$num_credito','$num_poliza','$requerimiento','$mot',curdate(),'$email','$telefono','$banco','$cuenta','Pendiente sin  Firma','$flujo')";
//echo $query;
$result = $mysqli->query($query);
$ultimoId = $mysqli->insert_id;
$html='<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Portal de Seguros</title>
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/css/postventa.css" rel="stylesheet" >
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="assets/js/jquery-function.js"></script>
    <script src="assets/js/modal.js" type="text/javascript"></script>
    <script>
        function enviar_formulario(){
            document.formulario1.submit()
        }
    </script> 
</head>
<body>
    <header>
        <div class="logo_itau">
           <img src="assets/img/logo-itau.png" alt="">

        </div>
        <div class="header_right">
            Plataforma Comercial
        </div>
    </header>
        <div class="container-firma">
        <div class="moduloFirma">
            <div class="tituloFirma">
                <h4 class="blanco">Eliminar seguro</h4>
            </div>
            <div class="contenido-firma">

                <!-- FIRMA clave ATM -->
                <div id="Control_ValidaPin"></div>
				<!-- FIN FIRMA clave ATM -->
                  <!-- Modal -->
                    <div id="myModal" class="modal">
                    <!-- Modal content -->
                        <div class="modal-content" style="border-radius: 5px">
                          <div class="modal-header" style="background-color: #332f83; border-radius: 5px 5px 0px 0px;">

                            <h2 class="font14">Cancelar requerimiento</h2>
                          </div>
                          <div class="modal-body flex-items-modal">
                            <div class="anchoIconModal margen-right-30">
                              <img src="assets/img/alerta.png" alt="">
                            </div>

                            <div class="anchoTextModal text-modal">
                              <p class="font14">¿Desea cancelar este nuevo requerimiento?</p>
                            </div>

                          </div>
                          <div class="modal-footer alinear-btn-modal-end" style="border-radius: 5px">

                          <button class="btn_gris btn_xs cerrar cerrarModal" type="button" style="margin-top: 0px; margin-right: 35px;">No</button>

                           <button type="button" class="btn_naranja btn_xs " onclick="window.location.href=../../07postventa.html" >Si</button> 

                          </div>
                        </div>
                  </div><!-- FIN Modal -->
              </div>
          </div>           
    </div>  
</body>
</html>';
//INSERT REQUERIMIENTO FIN
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Portal de Seguros</title>
    <meta http-equiv= "X-UA-Compatible" content="IE=9"/>
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/css/postventa.css" rel="stylesheet" >
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="assets/js/jquery-function.js"></script>
    <script src="assets/js/modal.js" type="text/javascript"></script>
    <script>
        function enviar_formulario(){
            document.formulario1.submit()
        }
    </script> 
</head>
<body >
    <header>
        <div class="logo_itau">
           <img src="assets/img/logo-itau.png" alt="">

        </div>
        <div class="header_right">
            Plataforma Comercial
        </div>
    </header>
    <div class="container_menu">
            <div class="menu">
                <div class="top_menu">
                    <h4>Portal de Seguros</h4>
                </div>
                <div class="menu_box">
                    <ul>
                        <li class="marginb1"><a href="">Información del cliente</a></li>
                        <li class="marginb1"><a href="">Seguros contratados</a></li>
                        <li><a href="07postventa.html"><strong class="menu_active">Post-venta</strong></a></li>
                    </ul>
                </div>
            </div>
        </div> 
        <div class="container-datos-datos">
        <div class="moduloPostVenta">
            <div class="titulo">
                <h4 class="blanco">Eliminar seguro</h4>
            </div>

  <div class="contenido">

     <div class="resumen">                   

    <div class="datos" style="margin-right: 50px; width: 28%;">
             <h4>Datos Contratante</h4>
             <ul class="nobullet nopadding marginl2">
               <li><strong>Nombre: </strong><?php echo $cliente_nombre;?></li>
               <li><strong>Rut: </strong><?php echo mostrar_rut(decrypt($rutCliente));?></li>
             </ul>
     </div>

     <div class="datos" style="margin-right: 50px; width: 28%;">
             <h4>Datos Requerimiento</h4>
             <ul class="nobullet nopadding marginl2">
                <?php
                $pliza = explode("$", $poliza);
                $fecha = explode("*-*",$pliza[1]);
                ?>
               <li><strong>Poliza: </strong><?php echo $pliza[0];?></li>
               <li><strong>Fecha: </strong><?php echo $fecha[0];?></li>
               <li><strong>Requerimiento: </strong><?php  echo buscar_requerimiento($requerimiento);?></li>
               <li><strong>Motivo: </strong><?php  echo buscar_motivo($motivo);?></li>
             </ul>
     </div>

     <div class="datos" style="width: 28%;">
       <h4>Datos Bancarios</h4>
       <ul class="nobullet nopadding marginl2" style="width: 92%;">
         <li><strong>Banco: </strong> <?php echo buscar_banco($banco);?></li>
         <li><strong>Nº de Cuenta: </strong> <?php echo $cuenta;?></li>
       </ul>
     </div>
 </div>

    <!-- Paso de variables a itaú -->
    <form action="<?php echo $UrlValidaPinItau;?>" method="post" onsubmit="return validaChk();" id="frmenvio" name="frmenvio">
    <div class="botones margint1">
    <button class="btn_gris btn_cancelar" id="myBtn" type="button" style="margin-right: 20px;">Cancelar</button>    
    <input type="hidden" name="AO_rut" id="AO_rut" value="<?php echo $rut; ?>" readonly="readonly">
    <input type="hidden" name="AO_user" id="AO_user" value="<?php echo $_SESSION['user']; ?>" readonly="readonly">
    <input type="hidden" name="AO_serverName" id="AO_serverName" value="<?php echo $_SESSION["serverName"]; ?>" readonly="readonly">  
    <input type="hidden" name="AO_Token" id="AO_Token" value="<?php echo $_SESSION["token"]; ?>" readonly="readonly">  
    <input type="hidden" name="AO_Incremental" id="AO_Incremental" value="<?php echo $_SESSION["incremental"]; ?>" readonly="readonly">
    <input type="hidden" name="AO_UserID" id="AO_UserID" value="<?php echo $_SESSION['userid']; ?>" readonly="readonly">
    <input type="hidden" name="AO_Apenom" id="AO_Apenom" value="<?php echo $seguro["apenom"]; ?>" readonly="readonly">  
    <input type="hidden" name="AO_Cargo" id="AO_Cargo" value="<?php echo $seguro["cargo"]; ?>" readonly="readonly">
    <input type="hidden" name="AO_Suc" id="AO_Suc" value="<?php echo $seguro["suc"]; ?>" readonly="readonly">
    <input type="hidden" name="AO_NomSuc" id="AO_NomSuc" value="<?php echo $seguro["nomsuc"]; ?>" readonly="readonly">      
    <input type="hidden" name="AO_CodigoSeguro" id="AO_CodigoSeguro" value="" readonly="readonly">
    <input type="hidden" name="AO_IDVenta" id="AO_IDVenta" value="" readonly="readonly">
    <input type="hidden" name="AO_RutEncriptado" id="AO_RutEncriptado" value="<?php echo $data; ?>" readonly="readonly">
    <input type="hidden" name="AO_UrlValidaPinItau" id="AO_UrlValidaPinItau" value="<?php echo $_SESSION['UrlValidaPinItau']; ?>" readonly="readonly">
      
    <input type="hidden" name="AO_HTMLPagina" id="AO_HTMLPagina" value="<?php echo base64_encode($html); ?>" readonly="readonly">
    <?php
        $volverdeItau=$_SESSION['UrlVolver']."contratacionSucursal.php?data=".$_GET['data']."&pol=".$_GET['pol']."&req=".$_GET['req']."&mot=".$_GET['mot']."&email=".$_GET['email']."&tel=".$_GET['tel']."&banco=".$_GET['banco']."&cuenta=".$_GET['cuenta'];
    ?>
    <input type="hidden" name="AO_UrlVolver" id="AO_UrlVolver" value="<?php echo $volverdeItau; ?>">
      
    <input type="hidden" name="AO_NombreSeguro" id="AO_NombreSeguro" value="" readonly="readonly">
    <input type="hidden" name="AO_RutEjecutivo" id="AO_RutEjecutivo" value="<?php echo $rutVendedor; ?>" readonly="readonly">
    <input type="hidden" name="AO_Prima" id="AO_Prima" value="" readonly="readonly">
  
    <input type="hidden" name="AO_TipoFlujo" id="AO_TipoFlujo" value="POSTVENTA" readonly="readonly">
    <input type="hidden" name="AO_NombreCliente" id="AO_NombreCliente" value="<?php echo $cliente_nombre;?>" readonly="readonly">
    <input type="hidden" name="AO_NombrePoliza" id="AO_NombrePoliza" value="<?php echo $ultimoId?>" readonly="readonly">
    <input type="hidden" name="AO_AsuntoConsulta" id="AO_AsuntoConsulta" value="<?php echo $requerimiento; ?>" readonly="readonly">
    <input type="hidden" name="AO_Motivo" id="AO_Motivo" value="<?php echo $mot;?>" readonly="readonly">
    <input type="hidden" name="AO_CorreoCliente" id="AO_CorreoCliente" value="<?php echo $email;?>" readonly="readonly">
    <input type="hidden" name="AO_TelefonoCliente" id="AO_TelefonoCliente" value="<?php echo $telefono;?>" readonly="readonly">
  
    <input type="hidden" name="AO_Banco" id="AO_Banco" value="<?php echo $banco;?>" readonly="readonly">
    <input type="hidden" name="AO_CtaCte" id="AO_CtaCte" value="<?php echo $cuenta;?>" readonly="readonly">
                    
    <input type="submit" id="boton" class="btn_naranja displayib btn_submit" onclick="validaChk();" value="Continuar">
                    

    </div>
</form>

                       
                <!-- FIN FASE 2 -->


<?php //include('pieUF.php');?>

<!-- Modal -->
<div id="myModal" class="modal">
  <!-- Modal content -->
  <div class="modal-content" style="border-radius: 5px">
    <div class="modal-header" style="background-color: #332f83; border-radius: 5px 5px 0px 0px;">
      
      <h2 class="font14">Cancelar Contratación</h2>
    </div>
    <div class="modal-body flex-items-modal">
      <div class="anchoIconModal margen-right-30">
        <img src="../../assets/img/alerta.png" alt="">
      </div>

      <div class="anchoTextModal text-modal">
        <p class="font14">¿Desea cancelar el flujo actual?</p>
      </div>
      
    </div>
    <div class="modal-footer alinear-btn-modal-end" style="border-radius: 5px">
      
      <button class="btn_gris btn_xs cerrar cerrarModal" type="button" style="margin-top: 0px; margin-right: 35px;">No</button>
      
      <button type="button" class="btn_naranja btn_xs " onclick="window.location.href='informacion.php?data=<?php echo $_GET['data']; ?>'" >Si</button> 

    </div>
  </div>

</div><!-- FIN Modal -->


</div>







          </div>           
    </div>  
</body>
</html>