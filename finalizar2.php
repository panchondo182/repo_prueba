<?php 


session_start();
    //echo $DB['host']." ". $DB['user']." ". $DB['pass']." ". $DB['useBI'];
$data = isset($_GET["data"]) ? $_GET["data"] : ''; 
$cargo = isset($_SESSION['cargo']) ? $_SESSION['cargo'] : '';

$req = isset($_GET["req"]) ? $_GET["req"] : ''; 
switch($req){
	case 'copia_de_poliza_o_certificado':
        $reque='Copia de p&oacute;liza';
        break;
	case 'eliminacion_de_seguro':
        $reque='Eliminaci&oacute;n de seguro';
        break;
	case 'devolucion_de_prima':
        $reque='Devoluci&oacute;n de prima';
        break;
}
$npol = isset($_GET["npol"]) ? $_GET["npol"] : ''; 

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Portal de Seguros</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9"/>
    <link href="assets/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="assets/css/estilos.css" rel="stylesheet" type="text/css">
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/css/postventa.css" rel="stylesheet" >
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="assets/js/jquery-function.js"></script>
    <script src="assets/js/modal.js" type="text/javascript"></script>
    <script>
        function enviar_formulario(){
            document.formulario1.submit()
        }
    </script> 
</head>
<body>
    <header>
        <div class="logo_itau">
           <img src="assets/img/logo-itau.png" alt="">

        </div>
        <div class="header_right">
            Plataforma Comercial
        </div>
    </header>
    <div class="container_menu">
            <div class="menu">
                <div class="top_menu">
                    <h4>Portal de Seguros</h4>
                </div>
                <div class="menu_box">
                    <ul>
                        <?php if($cargo!='BTEL'){ ?><li class="marginb1"><a href="informacion.php?data=<?php echo $data; ?>">Información del cliente</a></li> <?php } ?>                        <li class="marginb1"><a href="segurosContratados.php?data=<?php echo $data;?>">Seguros contratados</a></li>
                        <li><a href="postventa.php?data=<?php echo $data; ?>"><strong class="menu_active">Post-venta</strong></a></li>
                    </ul>
                </div>
            </div>
        </div> 
        <div class="container-datos-datos">
        <div class="moduloPostVenta">
            <div class="titulo">
                <h4 class="blanco">Disponibilidad en Canales Digitales</h4>
            </div>
            <div class="contenido">

                     <div>
                    <div class="message">
                        <img src="assets/img/canales-digitales.png" alt="" class="margins0">
                        <p class="text_center margintb"><strong>El requerimiento se ha habilitado exitosamente para confirmación en Canales Digitales</strong></p>
                        <p><strong>Importante informar al cliente:</strong></p>
                        <ul class="bullet">
                            <!--<li>El usuario puede finalizar a través del <strong>Sitio Privado</strong> en Web.</li>-->
                            <li>El cliente puede confirmar a través del <strong>Sitio Privado</strong> en Web.</li>
                            <li>Requerimiento con  <strong>vigencia de 48 horas.</strong></li>
                        </ul>
                        <a class="btn_naranja displayb margintb margins0" href="postventa.php?data=<?php echo $data;?>">Ir a Post-venta</a>
                    </div>
                </div>
 
              </div>
          </div>           
    </div>  
</body>
</html>